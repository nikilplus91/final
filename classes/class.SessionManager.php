<?php
//! Include Required Audit Class
include_once dirname(__FILE__).DIRECTORY_SEPARATOR."class.User.php";
/*!
 * @class SessionManager
 * This class manages the user session.
 */
    
class SessionManager {
    public $isLoggedIn;
    public $iUserID;
    public $sUsername;
    public $sName;
    public $sEmail;
    public $iMobile;
    public $sError;
    public $iUserTypeID;
    public $sUserType;
    public $iSessionDuration = SESSION_TIME; // In Min


    //constructor
    function __construct()
    {
        if(!isset($_SESSION)){
            session_start();
        }

        if(isset($_SESSION['isLoggedIn'])){
            if($_SESSION['isLoggedIn'] === TRUE){
                $this->isLoggedIn = TRUE;
                $this->iUserID=$_SESSION['iUserID'];
                $this->sUsername=$_SESSION['sUsername'];
                $this->sName=$_SESSION['sName'];
                $this->sEmail=$_SESSION['sEmail'];
                $this->iMobile=$_SESSION['iMobile'];
                $this->iUserTypeID=$_SESSION['iUserTypeID'];
                $this->sUserType=$_SESSION['sUserType'];
            }
            else {
                $this->isLoggedIn = FALSE;
            }
        }
    }
    
    //! @return boolean Boolean value stating whether current user is logged in or not
    function isLoggedIn()
    {
        return $this->isLoggedIn;

    }

    function fAuthenticateUser($sUsername, $sPassword)
    {
        $sUsername = trim($sUsername);
        $sPassword = trim($sPassword);
        if ($sUsername == '' || $sPassword == '')
        {
            $this->sError = INVALID_DATA;
            return FALSE;
        }
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sUsername = $conn->real_escape_string($sUsername);
        $sPassword = $conn->real_escape_string($sPassword);
        $sLoginTable = 'user_logins';
        $sQuery = "SELECT user_id,password FROM {$sLoginTable} WHERE username='{$sUsername}'";
		$result = $conn->query($sQuery);
		if(!$result){
            $this->sError = DATABASE_ERROR;
            return FALSE;
        }
        $aTemp = $result->fetch_array();
		
         if($aTemp[0]=='')
        {
            return false;
        }
        $sRealPass = $aTemp[1];
        $iUID = $aTemp[0];
        
        $verified = $this->fVerifyPassword($sPassword,$sRealPass);
        if($verified){
            $this->isLoggedIn = TRUE;
            $_SESSION['isLoggedIn']=TRUE;
            $this->iUserID=$iUID;
            $oUser = new User($this->iUserID);
            $this->sUsername=$oUser->sUsername;
            $_SESSION['sUsername'] = $this->sUsername;
            $this->iUserTypeID = $oUser->iUserTypeID;
            $_SESSION['iUserTypeID'] = $oUser->iUserTypeID;
            $this->sUserType = $oUser->sUserType;
            $_SESSION['sUserType'] = $oUser->sUserType;
            $this->sName=$oUser->sName;
            $this->sEmail=$oUser->sEmail;
            $this->iMobile=$oUser->iMobile;
            $_SESSION['sName']=$oUser->sName;
            $_SESSION['iUserID'] = $this->iUserID;
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    static function fVerifyPassword($sPlainPassword, $sPasswordHash)
    {
        $salt = substr($sPasswordHash,0,32);
        $hash = substr($sPasswordHash,32);
        $sSHAHash = hash("sha256", $salt.$sPlainPassword);
        if($sPasswordHash == $salt.$sSHAHash){
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function fLogOut(){

        /*  Additions Does - It unset the cookies of browser and removes the session data from server. */        
        // Unset cookies
        unset($_SESSION['isLoggedIn']);
        unset($_SESSION['sUsername']);
        unset($_SESSION['iUserTypeID']);
        unset($_SESSION['sUserType']);
        unset($_SESSION['sName']);
        
        header('Location: login.php');  
    }


    /*! @fn function fGetLoggedInUser()
    * @brief Give the logged in user
    * This function will return the object of User who is currently logged In.
    * @return object It will return the object of User class
    */
    function fGetLoggedInUser(){
        return new User($this->iUserID);
    }
}
?>