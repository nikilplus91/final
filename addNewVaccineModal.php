 <div class="modal fade" id="idModalAddNewVaccination" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Add New Vaccine</h3>
            </div>
            <div class="modal-body">
                <form id="idFormAddNewVaccine">
                    <div class="form-group">
                        <label for="name">Name <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="idNewVaccineName" name="idNewVaccineName" placeholder="Vaccine Name">
                    </div>
                    <div class="form-group">
                        <label for="email">No. of Days <span class="text-danger">*</span></label>
                        <input type="number" class="form-control" id="idNoOfDays" name="idNoOfDays" placeholder="After Number of Days">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary classAddNewVaccine">Submit</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        //! Show modal..
        $(document).on('click', '#idAddNewVaccine', function(){
            $("#idModalAddNewVaccination").modal('show');
        });

        //! Submit vaccine..
        $("#idModalAddNewVaccination").on('click', '.classAddNewVaccine', function(){
            var sVaccineName = $("#idNewVaccineName").val();
            var iNoOfDays = $("#idNoOfDays").val();

            //! Validation..
            if(sVaccineName == '' || iNoOfDays == ''){
                alert('Invalid details');
                return false;
            }

            data = new FormData($('#idFormAddNewVaccine')[0]);

            $.ajax({
                type: 'POST',
                url: 'ajax.manager.php?sFlag=add_new_vaccine',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                    if(data !== false){
                        alert('New vaccination added successfully');
                        $("#idModalAddNewVaccination").modal('hide');
                    }else{
                        alert('Something went wrong!');
                        return false;
                    }
                }
            });
        });
    });
</script>