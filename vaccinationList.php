<?php
include_once "config/configApp.php";
include_once ABS_PATH_TO_APP."functions.php";
include_once ABS_PATH_TO_APP."classes/class.SessionManager.php";

$oSessionManager = new SessionManager();
$iUserID = $oSessionManager->iUserID;
$iUserTypeID = $oSessionManager->iUserTypeID;

if($iUserTypeID == 3){
    $iParentID = fGetParentIDByUserID($iUserID);

    $aChildDetails = fGetChildDetailsByParentID($iParentID);    
}
//! Include configEhr.php file for constant variable values.
include_once "config/configApp.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo "Vaccination List"; ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="dist/css/style.css" rel="stylesheet" type="text/css">
    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->  

    <style type="text/css">
        .classToggleFilter{
            cursor: pointer;
        }
    </style>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#" style="margin-top: -11px;"><img src="<?php echo "images/".PROJECT_LOGO; ?>" style="height: 54px;"/></a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?php echo $oSessionManager->sName.' ('.$oSessionManager->sUsername.')'; ?>&nbsp;&nbsp;<i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <?php
                            if($oSessionManager->iUserTypeID == 3 && !empty($aChildDetails)){
                                ?>
                                <li><a href="#"><i class="fa fa-user fa-fw"></i> Child Details</a></li>
                                <?php
                                foreach ($aChildDetails as $iIndex => $aChild) {
                                    ?>
                                    <li><a href="#">Child : <strong><?php echo $aChild['child_no']; ?></a></strong></li>
                                    <?php
                                }
                            }
                        ?>
                        <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <?php
                        if($iUserTypeID == 3 && !empty($aChildDetails)){
                            ?>
                            <li>
                                <a href="vaccinationList.php"><i class="fa fa-dashboard fa-fw"></i> Vaccination History</a>
                            </li>                            
                            <li>
                                <a href="childSignUp.php"><i class="fa fa-user fa-fw"></i> Child Registration</a>
                            </li>
                            <?php
                        }else if($iUserTypeID == 1 || $iUserTypeID == 2){
                            ?>
                            <li>
                                <a href="vaccinationList.php"><i class="fa fa-user-md fa-fw"></i> Give Vaccine</a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" id="idAddNewVaccine"><i class="fa fa-plus fa-fw"></i> Add New Vaccine</a>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>
              
                <div class="vacimgclass">
               <?php
               include_once "fetch_weather.php";
               ?>
                </div>
                
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid bImg">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"><?php echo ($iUserTypeID == 3) ? "VACCINATION HISTORY" : "GIVE VACCINATION"; ?></h1>
                        <form role="form">
                            
                        </form>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="container-fluid">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                <label class="control-label">Child Number:</label>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <input type="text" class="form-control classChildNumber" id="idChildNumber" name="idChildNumber" placeholder="Enter Child Number" />
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <a class="btn btn-md btn-primary classSearchBtn" id="idSearchBtn"><i class="fa fa-search" aria-hidden="true"></i>&nbsp;&nbsp;Search</a>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="container-fluid classVaccinationListing">
                        
                    </div>
                </div>
                
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

    <script type="text/javascript">
        var iUserTypeID = "<?php echo $iUserTypeID; ?>";
        $(document).ready(function(){

            $(document).on('click', '.classSearchBtn', function(){
                var sChildNo = $("#idChildNumber").val();
                if(sChildNo == ''){
                    alert('Please enter child number.');
                    return false;
                }

                fGetVaccinationGroupDetails();
            });

            //toggle filter div
            $(document).on('click', '.classToggleFilter', function(){
                var $this = $(this);
                var sChildNo = $("#idChildNumber").val();

                if(!$this.hasClass('panel-collapsed')) {
                    $this.parent().find('.classPanelBody').hide(666);
                    $this.addClass('panel-collapsed');
                    $this.find('#idBtnChevron').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
                    $this.parent().find('.classPanelBody').html('');
                } else {
                    var iMinAge = $.trim($(this).attr('data-group-min-age'));
                    var iMaxAge = $.trim($(this).attr('data-group-max-age'));
                    
                    fGetVaccinationListByAge(iMinAge,iMaxAge,$this,sChildNo);
                    $this.parent().find('.classPanelBody').show(666);
                    $this.removeClass('panel-collapsed');
                    $this.find('#idBtnChevron').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
                }
            });

            $(document).on('click', '.classGiveVaccine', function(){
                var iVaccinationID = $(this).data('vaccination-id');

                var dReceivedDate = $("#idReceivedDate_"+iVaccinationID).val();
                var sRemark = $("#idRemark_"+iVaccinationID).val();
                var sChildNo = $("#idChildNumber").val();
                var iUserID = $("#idUserID").val();

                //! Validation..
                if(dReceivedDate == ''){
                    alert('Please select received date');
                    return false;
                }

                //! Validation..
                if(sChildNo == ''){
                    alert('Please select child number.');
                    return false;
                }

                $.ajax({
                    url: "ajax.manager.php?sFlag=add_vaccine",
                    data: {iVaccinationID:iVaccinationID,dReceivedDate:dReceivedDate,sRemark:sRemark,sChildNo:sChildNo,iUserID:iUserID},
                    async: false,
                    success: function (data){                       
                        if(data !== false){
                            $(".classSearchBtn").trigger('click');
                        }else{
                            alert('Something went wrong!');
                            return false;
                        }
                    }
                });
            });

            //! Function for fetching vaccination listing by child id..
            function fGetVaccinationGroupDetails(){
                $.ajax({
                    url: "ajax.manager.php?sFlag=vaccination_group_details",
                    async: false,
                    success: function (data){
                        var sTemplate = '';
                        if($.trim(data) != false){
                            sTemplate = '<input type="hidden" name="idUserID" id="idUserID" value="<?php echo $iUserID;?>">';
                            $.each(data,function(iIndex,aGroup){
                                sTemplate +='<div class="panel panel-primary">';
                                    sTemplate +='<div class="panel-heading classToggleFilter panel-collapsed" data-group-id="'+aGroup.group_id+'" data-group-min-age="'+aGroup.min_age+'" data-group-max-age="'+aGroup.max_age+'">';
                                        sTemplate +='<i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;&nbsp;<strong>'+aGroup.group_name+'</strong>';
                                        sTemplate +='<span class="classCollapseButton pull-right"><i id="idBtnChevron" class="glyphicon glyphicon-chevron-down"></i></span>';
                                    sTemplate +='</div>';
                                    sTemplate +='<div class="panel-body classPanelBody" style="display: none;">';
                                        
                                    sTemplate +='</div>';
                               sTemplate += '</div>';
                            });

                            $(document).find(".classVaccinationListing").html(sTemplate);
                        }
                    }
                });
            }

            //! Function for fetching vaccination listing by child id..
            function fGetVaccinationListByAge(iMinAge,iMaxAge,sThis,sChildNo){
                $.ajax({
                    url: "ajax.manager.php?sFlag=vaccination_list_by_age",
                    data:{iMinAge:iMinAge,iMaxAge:iMaxAge,sChildNo:sChildNo},
                    async: false,
                    success: function (data){
                        var sTemplate = '';
                        if($.trim(data) != false){
                            sTemplate +='<table class="table table-hover table-responsive table-condensed">';
                                sTemplate +='<thead style="color:#884646;"> ';
                                    sTemplate +='<tr>';
                                        sTemplate +='<th width="5%">#</th>';
                                        sTemplate +='<th width="20%">Vaccine Name</th>';
                                        sTemplate +='<th width="20%">Ideal Date</th>';
                                        sTemplate +='<th width="20%">Received Date</th>';
                                        sTemplate +='<th width="25%">Remark</th>';
                                        if(iUserTypeID != 3){
                                            sTemplate +='<th width="5%">Action</th>';
                                        }
                                    sTemplate +='</tr>';
                                sTemplate +='</thead>';
                                sTemplate +='<tbody>';
                                $.each(data,function(iIndex,aVaccination){
                                    var sStyle = "";
                                    if(aVaccination.received_date != ''){
                                        sStyle = 'style="background-color: cadetblue;"';                                            
                                    }
                                    sTemplate += '<tr '+sStyle+'>';
                                        sTemplate += '<td>'+(iIndex+1)+'</td>';
                                        sTemplate += '<td>'+(aVaccination.vaccination_name)+'</td>';
                                        if(iUserTypeID == 3){
                                            sTemplate += '<td>'+aVaccination.ideal_date+'</td>';
                                        }else{
                                            sTemplate += '<td><input type="text" class="form-control classIdealDate" id="idIdealDate_'+aVaccination.vaccination_id+'" name="idIdealDate" data-vaccination-id="'+aVaccination.vaccination_id+'" value="'+aVaccination.ideal_date+'"/></td>';
                                        }                                        
                                        if(iUserTypeID == 3){
                                            sTemplate += '<td>'+(aVaccination.received_date == '' ? 'NA' : aVaccination.received_date)+'</td>';
                                        }else{
                                            sTemplate += '<td><input type="text" class="form-control classReceivedDate" id="idReceivedDate_'+aVaccination.vaccination_id+'" name="idReceivedDate" data-vaccination-id="'+aVaccination.vaccination_id+'" value="'+aVaccination.received_date+'"/></td>';
                                        }
                                        if(iUserTypeID == 3){
                                            sTemplate += '<td>'+(aVaccination.remark == '' ? 'NA' : aVaccination.remark)+'</td>';
                                        }else{
                                            sTemplate += '<td><textarea class="form-control classRemark" id="idRemark" name="idRemark_'+aVaccination.vaccination_id+'">'+aVaccination.remark+'</textarea></td>';
                                        }
                                        if(iUserTypeID != 3){
                                            if(aVaccination.received_date != ''){
                                                sTemplate += '<td>Given</td>';                                            
                                            }else{
                                                sTemplate += '<td><a class="btn btn-sm btn-success classGiveVaccine" data-vaccination-id="'+aVaccination.vaccination_id+'" id="idGiveVaccine_'+aVaccination.vaccination_id+'">Give Vaccine</a></td>';                                            
                                            }
                                        }
                                    sTemplate += '</tr>';
                                });
                                sTemplate +='</tbody>';
                            sTemplate +='</table>';

                            $(sThis).parent().find('.classPanelBody').html(sTemplate);
                        }
                    }
                });
            }
        });
    </script>
    <?php include_once "addNewVaccineModal.php"; ?>
</body>

</html>