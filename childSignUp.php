<?php
include_once "config/configApp.php";
include_once ABS_PATH_TO_APP."functions.php";
include_once ABS_PATH_TO_APP."classes/class.SessionManager.php";

$oSessionManager = new SessionManager();
$iUserID = $oSessionManager->iUserID;
$iUserTypeID = $oSessionManager->iUserTypeID;

if($iUserTypeID == 3){
    $iParentID = fGetParentIDByUserID($iUserID);

    $aChildDetails = fGetChildDetailsByParentID($iParentID);    
}

$sChildNumber = fGetChildUniqueNumber();

//! Include configEhr.php file for constant variable values.
include_once "config/configApp.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo "Vaccination List"; ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="dist/css/style.css" rel="stylesheet" type="text/css">
    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->  

    <style type="text/css">
        .classToggleFilter{
            cursor: pointer;
        }
    </style>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#" style="margin-top: -11px;margin-left: 46px;"><img src="<?php echo "images/".PROJECT_LOGO; ?>" style="height: 43px;"/></a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?php echo $oSessionManager->sName.' ('.$oSessionManager->sUsername.')'; ?>&nbsp;&nbsp;<i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <?php
                            if($oSessionManager->iUserTypeID == 3 && !empty($aChildDetails)){
                                foreach ($aChildDetails as $iIndex => $aChild) {
                                    ?>
                                    <li><a href="#"><i class="fa fa-user fa-fw"></i> Child <?php echo ($iIndex+1); ?> : <?php echo $aChild['child_no']; ?></a></li>
                                    <?php
                                }
                            }
                        ?>
                        <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <?php
                        if($iUserTypeID == 3 && !empty($aChildDetails)){
                            ?>
                            <li>
                                <a href="vaccinationList.php"><i class="fa fa-dashboard fa-fw"></i> Vaccination History</a>
                            </li>                            
                            <li>
                                <a href="childSignUp.php"><i class="fa fa-user fa-fw"></i> Child Registration</a>
                            </li>
                            <?php
                        }else if($iUserTypeID == 1 || $iUserTypeID == 2){
                            ?>
                            <li>
                                <a href="vaccinationList.php"><i class="fa fa-user-md fa-fw"></i> Give Vaccine</a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" id="idAddNewVaccine"><i class="fa fa-plus fa-fw"></i> Add New Vaccine</a>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"><?php echo "CHILD SIGN UP"; ?></h1>
                        <form role="form">
                            
                        </form>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <form id="idFormChildRegistation">
                    <input type="hidden" name="idUserID" id="idUserID" value="<?php echo $iUserID;?>">
                    <div class="row">
                        <div class="form-group">
                            <label for="number">Registration Number <span class="text-danger">*</span></label>
                            <input type="text" class="form-control classChildUniqueNumber" data-row-id="1" id="idChildUniqueNumber_1" name="idChildUniqueNumber[]" placeholder="Child Unique Number" readonly="" style="width:300px;" value="<?php echo $sChildNumber; ?>"/>
                        </div>
                        <div class="form-group">
                            <label for="name">Name <span class="text-danger">*</span></label>
                            <input type="text" class="form-control classChildName" data-row-id="1" id="idChildName_1" name="idChildName[]" placeholder="Child Name" style="width:300px;"/>
                        </div>
                        <div class="form-group">
                            <label for="name">DOB <span class="text-danger">*</span></label>
                            <input type="text" class="form-control classChildDOB" data-row-id="1" id="idChildDOB_1" name="idChildDOB[]" placeholder="Child DOB" style="width:300px;"/>
                        </div>
                    </div>
                    <hr>                    
                </form>

                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <a class="btn btn-primary btn-md classAddNewChild" id="AddNewChild">Add New Child Registration</a>
                        <a class="btn btn-success btn-md classSubmitChildRegistration" id="SubmitChildRegistration">Submit</a>
                    </div>
                </div>
                
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

    <script type="text/javascript">
        var iIndex = 2;
        var iUserTypeID = "<?php echo $iUserTypeID; ?>";
        $(document).ready(function(){

            var blankClone = $([
                '<div class="row">',
                    '<div class="form-group">',
                        '<label for="number">Registration Number <span class="text-danger">*</span></label>',
                        '<input type="text" class="form-control classChildUniqueNumber" data-row-id="'+iIndex+'" id="idChildUniqueNumber_'+iIndex+'" name="idChildUniqueNumber[]" placeholder="Child Unique Number" readonly="" style="width:300px;" />',
                    '</div>',
                    '<div class="form-group">',
                        '<label for="name">Name <span class="text-danger">*</span></label>',
                        '<input type="text" class="form-control classChildName" data-row-id="'+iIndex+'" id="idChildName_'+iIndex+'" name="idChildName[]" placeholder="Child Name" style="width:300px;"/>',
                    '</div>',
                    '<div class="form-group">',
                        '<label for="name">DOB <span class="text-danger">*</span></label>',
                        '<input type="text" class="form-control classChildDOB" data-row-id="'+iIndex+'" id="idChildDOB_'+iIndex+'" name="idChildDOB[]" placeholder="Child DOB" style="width:300px;"/>',
                    '</div>',
                    '<div class="form-group">',
                        '<a class="btn btn-danger btn-sm classRemoveChildRegistration">Remove</a>',
                    '</div>',
                '</div>',
                '<hr> '
            ].join(""));

            $(document).on('click', '.classAddNewChild', function(){
                var iClone = blankClone.clone();
                $("#idFormChildRegistation").append(iClone);

                fGetChildUniqueNumber(iIndex);

                iIndex++;
            });

            $(document).on('click', '.classRemoveChildRegistration', function(){
                $(this).parent().parent().remove();
            });

            //toggle filter div
            $(document).on('click', '.classToggleFilter', function(){
                var $this = $(this);
                if(!$this.hasClass('panel-collapsed')) {
                    $this.parent().find('.classPanelBody').show(666);
                    $this.addClass('panel-collapsed');
                    $this.find('#idBtnChevron').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
                } else {
                    $this.parent().find('.classPanelBody').hide(666);
                    $this.removeClass('panel-collapsed');
                    $this.find('#idBtnChevron').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
                }
            });

            $(document).on('click', '.classSubmitChildRegistration', function(){

                data = new FormData($('#idFormChildRegistation')[0]);

                $.ajax({
                    type: 'POST',
                    url: 'ajax.manager.php?sFlag=child_registration',
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        if(data !== false){
                            window.location.href="vaccinationList.php";
                        }else{
                            alert('Something went wrong!');
                            return false;
                        }
                    }
                });
            });

        });
        //! Function for fetching vaccination listing by child id..
        function fGetChildUniqueNumber(iRowID){
            $.ajax({
                url: "ajax.manager.php?sFlag=child_unique_number",
                async: false,
                success: function (data){                       
                    if(data != ""){
                        $(document).find("#idChildUniqueNumber_"+iRowID).val(data);
                    }
                }
            });
        }
    </script>

    <?php include_once "addNewVaccineModal.php"; ?>
</body>

</html>