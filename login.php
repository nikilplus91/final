<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>


    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="dist/css/style.css" rel="stylesheet" type="text/css">
    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

     
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="backgroundWrapper">

    <div class="container">
        <div class="row">
             <div class="loginWrapper">
                <div class="col-md-offset-3 col-md-6 col-xs-12">
                    <form class="form-container" action="doLogin.php" method="post">
                        <div class="formHeader">
                            <h2>Let's Login</h2>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Username</label>
                            <input type="text" class="form-control" id="idInputUsername" name="username" placeholder="Username">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" class="form-control" id="idInputPassword" name="password" placeholder="Password">
                        </div>
                        <button type="submit" class="btn btn-success btn-lg btn-block classSubmitLoginDetails">Submit</button>
                        <div class="form-group">
                            <a href="#" class="help-block classRegistrationUser">Don't have an account?</a>
                        </div>
                   </form>
                </div>
            </div> 
        </div>
    </div>

    <div class="modal fade" id="idModalRegistrationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">Registration</h3>
                </div>
                <div class="modal-body">
                    <form id="idFormUserRegistration">
                        <div class="form-group">
                            <label for="name">Name <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="idName" name="idName" placeholder="Name">
                        </div>
                        <div class="form-group">
                            <label for="email">Email <span class="text-danger">*</span></label>
                            <input type="email" class="form-control" id="idUserEmail" name="idUserEmail" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label for="mobile">Mobile <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="idUserMobile" name="idUserMobile" placeholder="Mobile">
                        </div>
                        <div class="form-group">
                            <label for="city">City <span class="text-danger">*</span></label>
                            <select class="form-control" id="idUserCity" name="idUserCity"></select>
                        </div>
                        <div class="form-group">
                            <label for="city">State <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="idUserState" name="idUserState" placeholder="State" readonly="">
                        </div>
                        <div class="form-group">
                            <label for="city">Country <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="idUserCountry" name="idUserCountry" placeholder="Country" readonly="">
                        </div>
                        <div class="form-group">
                            <label for="city">Username <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="idUserName" name="idUserName" placeholder="Username">
                        </div>
                        <div class="form-group">
                            <label for="password">Password <span class="text-danger">*</span></label>
                            <input type="password" class="form-control" id="idUserPassword" name="idUserPassword" placeholder="Password">
                        </div>
                        <div class="form-group">
                            <label for="confirm_password">Confirm Password <span class="text-danger">*</span></label>
                            <input type="password" class="form-control" id="idUserConfirmPassword" name="idUserConfirmPassword" placeholder="Confirm Password">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary classSubmitRegistration">Submit</button>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            fGetCityList();

            $(document).on('click', '.classRegistrationUser', function(){
                $("#idModalRegistrationModal").modal('show');
            });

            $(document).on('click', '.classSubmitLoginDetails', function(){
                var sUsername = $("#idInputUsername").val(),
                    sPassword = $("#idInputPassword").val();

                if(sUserName == '' || sUserName == null || sPassword == '' || sPassword == null){
                    alert('Please enter login details.');
                    return false;
                }
            });

            $(document).on('click', '.classSubmitRegistration', function(){
                var sUserName = $("#idUserName").val(),
                    sUserMobile = $("#idUserMobile").val(),
                    sUserEmail = $("#idUserEmail").val();
                    sUserCity = $("#idUserCity").val();
                    sUserState = $("#idUserState").val();
                    sUserCountry = $("#idUserCountry").val(),
                    sName = $("#idName").val(),
                    sPassword = $("#idUserPassword").val(),
                    sConfirmPasword = $("#idUserConfirmPassword").val();

                if(sUserName == '' || sUserName == null || sUserMobile == '' || sUserMobile == null || sUserEmail == '' || sUserEmail == null || sUserCity == '' || sUserCity == null || sUserState == '' || sUserState == null || sUserCountry == '' || sUserCountry == null || sName == '' || sName == null || sPassword == '' || sPassword == null || sConfirmPasword == '' || sConfirmPasword == null){
                    alert('Please fill all mandatory details');
                    return false;
                }

                if(sPassword != sConfirmPasword){
                    alert('Password is not matching. Please enter valid password');
                    return false;
                }

                data = new FormData($('#idFormUserRegistration')[0]);

                $.ajax({
                    type: 'POST',
                    url: 'ajax.manager.php?sFlag=user_registration',
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        if(data !== false){
                            $.post('doLogin.php', {
                                username: sUserName,
                                password: sPassword,
                                appRequest: 1
                            },function(data) {                        
                                if(data == 1){
                                    window.location.href="vaccinationList.php";
                                }else if(data == 2){
                                    window.location.href="childSignUp.php";
                                }else if(data == 3){
                                    alert('Something went wrong.');
                                    $("#idModalRegistrationModal").modal('hide');
                                    return false;
                                } else {
                                    alert('Something went wrong.');
                                    $("#idModalRegistrationModal").modal('hide');
                                    return false;
                                }
                            });
                        }else{
                            alert('Something went wrong!');
                            $("#idModalRegistrationModal").modal('hide');
                            return false;
                        }
                    }
                });
            });

            $(document).on('change', '#idUserCity', function(){
                $("#idUserState").val('');
                $("#idUserCountry").val('');
                if($(this).val() != ''){
                    fGetStateCountryByCity($(this).val());
                }
            });
        });

        function fGetCityList(){
            $.ajax({
                url: "ajax.manager.php?sFlag=city_list",
                async: false,
                success: function (data){                       
                    if($.trim(data) != false){
                        $("#idUserCity").append('<option>Select Any</option>');
                        $.each(data,function(iIndex,aCity){
                            $('#idUserCity').append($("<option></option>").attr("value",aCity.city).text(aCity.city));
                        });
                    }
                }
            });
        }

        function fGetStateCountryByCity(sCity){
            $.ajax({
                url: "ajax.manager.php?sFlag=state_country_by_city",
                data: {sCity:sCity},
                async: false,
                success: function (data){                       
                    if($.trim(data) != false){
                        $("#idUserState").val(data['state']);
                        $("#idUserCountry").val(data['country']);
                    }
                }
            });
        }
    </script>
</body>

</html>
