<?php

/*! Include @file class.DBConnManager.php for DBConnManager class */
include_once 'class.DBConnManager.php';

/*!
 * @class User
 * @brief This class represents a user and it's behavior. 
 * @author Kishan Gor
 */

class User
{
    //! user_id of User
    public $iID; 
    //! Name of the User
    public $sName; 
    //! Email of the User
    public $sEmail; 
    public $iMobile; 
    public $sUsername;
    public $iLoginID;
    public $iUserTypeID;
    public $sUserType;
    public $sCreatedOn;
    private $sPassword;
    public $sLastError;


    /*! @brief initialize the User class
    *  This class will instantiate the User class according to passed \a $iID representing the User ID.
    * @param $iID User ID
    * @note If no @a $iID is passed, it will not load the values and work as an skeleton for new User.
    *
    */
    function __construct($iID = NULL)
    {
        if($iID !== NULL){
            $this->iID = $iID;
            $DBMan = new DBConnManager();
            $conn =  $DBMan->getConnInstance();
            $sProfileTable = 'user_profiles';
            $sLoginTable = 'user_logins';
            $sUserTypeTable = 'user_types';

            $query = "SELECT * FROM `{$sProfileTable}` a, `{$sLoginTable}` b, `{$sUserTypeTable}` c WHERE a.user_id={$this->iID} AND b.user_id=a.user_id AND a.user_type_id=c.user_type_id";

             
            $result = $conn->query($query);
            if($result!==FALSE){
                $row = $result->fetch_array();
            }
            $this->sName = $row['name'];
            $this->sEmail = $row['email'];
            $this->iMobile = $row['mobile'];
            $this->sUsername = $row['username'];
            $this->iLoginID = $row['login_id'];
            $this->iUserTypeID = $row['user_type_id'];
            $this->sUserType = $row['user_type'];
            $this->sCreatedOn = $row['createdOn'];
            $this->sPassword = $row['password'];
            $this->sLastError = NULL;
            //! Destruct the DBConnManager Instance;
            $DBMan = NULL;
        }
    }

    /*! @brief adds the user as new user
    *  Calling this function will create a new user. Once we have set the user details, we can call this function and add its entry in the database.
    * @param $passoword Password for the new user.
    * @return boolean It will return true if user is added successfully. On failure, it will return error.
    * @warning If user id is set, it will not do anything, but return the false. 
    */
    function fAddUser($passoword) {
        if($this->iID != NULL){
            $this->sLastError = USER_ALREADY_EXISTS;
            return FALSE;
        }
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sLoginTable = 'user_logins';
        $sQuery = "SELECT COUNT(*) FROM {$sLoginTable} WHERE username='{$this->sUsername}'";
        $rResult = $conn->query($sQuery);
        if($rResult){
            $iCount= $rResult->fetch_array();
            if($iCount[0] > 0){
                $this->sLastError = DUPLICATE_USER_NAME;
                return FALSE;
            }
        }
        else {
            $this->sLastError = DATABASE_ERROR;
            return FALSE;
        }
        $sProfileTable = 'user_profiles';
        $sLoginTable = 'user_logins';
        $sCreatedOn = date('Y-m-d H:i:s');
        $sQuery = "INSERT INTO `{$sProfileTable}` (`user_id`, `login_id`, `name`, `email`, `mobile`, `user_type_id`, `createdOn`) 
            VALUES (NULL, '0', '{$this->sName}', '{$this->sEmail}', '{$this->iMobile}', '{$this->iUserTypeID}', '{$sCreatedOn}');";
        $conn1 = $DBMan->getConnInstance(); 
        $sResult = $conn1->query($sQuery);        
        if(!$sResult){
           $this->sLastError = DATABASE_ERROR;
            return FALSE;
        }
        $this->iID = $conn1->insert_id;
        $this->sPassword = $this->fGetPasswordHash($passoword);

        $sQuery = "INSERT INTO `{$sLoginTable}` (`login_id`, `username`, `password`, `user_id`)
            VALUES (NULL, '{$this->sUsername}', '{$this->sPassword}', '{$this->iID}');";
        $conn2 = $DBMan->getConnInstance(); //! Get a new db connection
        $sResult = $conn2->query($sQuery);
        if(!$sResult){
            $sTemp= "DELETE * FROM `{$sProfileTable}` WHERE user_id={$this->iID} ";
            $this->sLastError = DATABASE_ERROR;
            return FALSE;
        }
        $this->iLoginID = $conn2->insert_id;

        $sQuery = "UPDATE `{$sProfileTable}` SET `login_id` = {$this->iLoginID} WHERE user_id={$this->iID}";
        $conn3 = $DBMan->getConnInstance(); //! Get a new db connection
        $sResult = $conn3->query($sQuery);

        //! Destory Database object
        $DBMan = null;        

        return $this->iID;
    }

    /*! #brief generates the password hash
    * @arg $plainPassword 
    * @return string returns the hash of @a $plainPassword with 92 characters
    */
    private function fGetPasswordHash($plainPassword){
        $salt = md5(uniqid('', true));
        $sSHAHash = hash("sha256", $salt.$plainPassword);
        $sEncryptedPassword = $salt.$sSHAHash;
        return $sEncryptedPassword;
    }

    //! brief function to get all userd list
    function fGetAllUserList(){

        $aUserList = array();
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sProfileTable = 'user_profiles';
        
        $sQuery = "SELECT `user_id`, `name` FROM `{$sProfileTable}`";
        $sQueryR = $conn->query($sQuery);
        if($sQueryR!==FALSE){
            while($aRow = $sQueryR->fetch_assoc()){
                $aUserList[] = $aRow;
            }
        }

        return $aUserList;
    }

    //! brief function to get all users for user type
    function fGetUsersForUserType($iUserTypeID){

        $aUserList = array();
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sProfileTable = 'user_profiles';
        $sUserTypeTable = 'user_type';
        
        $sQuery = "SELECT A.`user_id`, A.`name`, A.`email`, B.`user_type` FROM `{$sProfileTable}` AS A LEFT JOIN `{$sUserTypeTable}` AS B ON B.`user_type_id` = A.`user_type_id` WHERE A.`user_type_id` in ({$iUserTypeID})";

        $sQueryR = $conn->query($sQuery);
        if($sQueryR!==FALSE){
            while($aRow = $sQueryR->fetch_assoc()){                
                $aUserList[] = $aRow;
            }
        }

        return $aUserList;
    }

    /*! @brief adds the user as new user
    *  Calling this function will create a new user. Once we have set the user details, we can call this function and add its entry in the database.
    * @param $passoword Password for the new user.
    * @return boolean It will return true if user is added successfully. On failure, it will return error.
    * @warning If user id is set, it will not do anything, but return the false. 
    */
    function addUser($passoword) {
        if($this->iID != NULL){
            $this->sLastError = USER_ALREADY_EXISTS;
            return FALSE;
        }
        $DBMan = new DBConnManager(CPH_DATABASE);
        $conn =  $DBMan->getConnInstance();
        $sLoginTable = 'cph_user_logins';
        $sQuery = "SELECT COUNT(*) FROM {$sLoginTable} WHERE username='{$this->sUsername}'";
        $rResult = $conn->query($sQuery);
        if($rResult){
            $iCount= $rResult->fetch_array();
            if($iCount[0] > 0){
                $this->sLastError = DUPLICATE_USER_NAME;
                return FALSE;
            }
        }
        else {
            $this->sLastError = DATABASE_ERROR;
            return FALSE;
        }
        $sProfileTable = 'cph_user_profiles';
        $sLoginTable = 'cph_user_logins';
        $sCreatedOn = date('Y-m-d H:i:s');
        $sQuery = "INSERT INTO `{$sProfileTable}` (`user_id`, `login_id`, `name`, `email`, `type`, `createdOn`) 
            VALUES (NULL, '0', '{$this->sName}', '{$this->sEmail}', '{$this->iType}', '{$sCreatedOn}');";
        $conn1 = $DBMan->getConnInstance(); 
        $sResult = $conn1->query($sQuery);
        if(!$sResult){
           $this->sLastError = DATABASE_ERROR;
            return FALSE;
        }
        $this->iID = $conn1->insert_id;
        $this->sPassword = $this->getPasswordHash($passoword);

        $sQuery = "INSERT INTO `{$sLoginTable}` (`login_id`, `username`, `password`, `user_id`)
            VALUES (NULL, '{$this->sUsername}', '{$this->sPassword}', '{$this->iID}');";
        $conn2 = $DBMan->getConnInstance(); //! Get a new db connection
        $sResult = $conn2->query($sQuery);
        if(!$sResult){
            $sTemp= "DELETE * FROM `{$sProfileTable}` WHERE user_id={$this->iID} ";
            $this->sLastError = DATABASE_ERROR;
            return FALSE;
        }
        $this->iLoginID = $conn2->insert_id;

        $sQuery = "UPDATE `{$sProfileTable}` SET `login_id` = {$this->iLoginID} WHERE user_id={$this->iID}";
        $conn3 = $DBMan->getConnInstance(); //! Get a new db connection
        $sResult = $conn3->query($sQuery);

        //! Destory Database object
        $DBMan = null;

        return $this->iID;
    }

    /*! #brief generates the password hash
    * @arg $plainPassword 
    * @return string returns the hash of @a $plainPassword with 92 characters
    */
    private function getPasswordHash($plainPassword){
        $salt = md5(uniqid('', true));
        $sSHAHash = hash("sha256", $salt.$plainPassword);
        $sEncryptedPassword = $salt.$sSHAHash;
        return $sEncryptedPassword;
    }

    //! brief function to get all users by name string..
    function searchUserByName($sUserName){

        $aUserList = array();
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sProfileTable = 'mxcel_user_profiles';
        
        $sQuery = "SELECT `user_id`, `name` FROM `{$sProfileTable}` WHERE `name` LIKE '{$sUserName}%' AND `user_type_id` !=1";
        
        $sQueryR = $conn->query($sQuery);
        if($sQueryR!==FALSE){
            while($aRow = $sQueryR->fetch_assoc()){
                $aUserList[] = $aRow;
            }
        }

        return $aUserList;
    }

    /*
        Function to get all user types
        Added by Akshay Sutar on 06-03-2018
    */
    function getAllUserTypes($aFilter){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable="mxcel_user_type";

        $sQuery = " SELECT * FROM `{$sTable}` ";
        
        // search by user type name
        if($aFilter['sUserType']!=''){
            $sUserType = $aFilter['sUserType']; 
            $sUserType = trim($sUserType);
            $sQuery .= " WHERE `user_type` LIKE '%{$sUserType}%'";
        }
        // search by user type id
        if($aFilter['sUserTypeId']!=''){
            $sUserTypeId = $aFilter['sUserTypeId'];
            $sQuery .= " WHERE `user_type_id` = '{$sUserTypeId}'";
        }

        $aData = array();
        $sQueryR = $conn->query($sQuery);
        if($sQuery){
            while($aRow = $sQueryR->fetch_array()){
                $aData[] = $aRow;
            }
        }
        return $aData;
    }

    /*
        Funtion to authenticate user
        Created by Akshay Sutar on 06-08-2018
    */
    public static function authenticateUser($sUserName,$sPassword){
        $sUserName = trim($sUserName);
        $sPassword = trim($sPassword);

        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sUserName = $conn->real_escape_string($sUserName);
        $sPassword = $conn->real_escape_string($sPassword);
        $sTable = 'user_logins';
        $iUserId = 0;

        $sQuery = "SELECT `user_id`,`password` FROM `{$sTable}` WHERE `username`='{$sUserName}'";
        $sQueryR = $conn->query($sQuery);

        if($sQueryR){
            $aData = $sQueryR->fetch_array();
            $sHashedPassword = $aData[1];
            $iThisUserId = $aData[0];

            $sSalt = substr($sHashedPassword,0,32);
            $sHash = substr($sHashedPassword,32);
            $sSHAHash = hash("sha256", $sSalt.$sPassword);
            if($sHashedPassword == $sSalt.$sSHAHash){
                $iUserId = $iThisUserId;
            }
            else {
                $iUserId = 0;
            }
        }
        return $iUserId;
    }

    /*
        Function to get User login password
        Created by Akshay Sutar on 27-11-2018
    */
    public function getUserLoginPassword(){
        return $this->sPassword;
    }

    public function fAddParentDetails($iUserID,$iNoOfChilds,$sAddress,$sCity,$sState,$sCountry,$sRegion){
        $DBMan = new DBConnManager();
        $conn = $DBMan->getConnInstance(); 

        $sParentTable = 'parent_details';
        $dAddedOn = date('Y-m-d H:i:s');

        $sQuery = "INSERT INTO `{$sParentTable}` (`parent_id`,`user_id`,`no_of_childs`,`address`,`city`,`state`,`country`,`region`,`added_on`,`deleted`) 
            VALUES (NULL, '{$iUserID}', '{$iNoOfChilds}', '{$sAddress}', '{$sCity}', '{$sState}', '{$sCountry}', '{$sRegion}', '{$dAddedOn}', '0')";
        
        $sResult = $conn->query($sQuery);        
        if(!$sResult){
           return false;
        }

        return $conn->insert_id;
    }
}

?>