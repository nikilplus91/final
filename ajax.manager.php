<?php
include_once "config/configApp.php";
include_once ABS_PATH_TO_APP."functions.php";
include_once ABS_PATH_TO_APP."classes/class.User.php";
include_once ABS_PATH_TO_APP."classes/class.SessionManager.php";
include_once ABS_PATH_TO_APP."classes/class.DBConnManager.php";

$sFlag = isset($_REQUEST['sFlag']) ? $_REQUEST['sFlag'] : '0';

if($sFlag == '0'){
	echo false;
}

switch ($sFlag) {
	//! search child number
	case 'search_child_number':
		$aChildNumber = array();

		$sKeyword = isset($_REQUEST['sKeyword']) ? addslashes($_REQUEST['sKeyword']) : '';

		if($sKeyword != ''){
			//! Fetching child number by keyword..
			$aChildNumber = searchChildNumber($sKeyword);
		}

		header("Content-Type: application/json");
		echo(json_encode($aChildNumber));
		break;

	//! get city list
	case 'city_list':
		$aCityList = array();

		//! Fetching child number by keyword..
		$aCityList = cityList();

		header("Content-Type: application/json");
		echo(json_encode($aCityList));
		break;

	//! get state & country by city
	case 'state_country_by_city':
		$aStateCountryList = array();

		$sCity = isset($_REQUEST['sCity']) ? addslashes($_REQUEST['sCity']) : '';

		if($sCity != ''){
			//! Fetching child number by keyword..
			$aStateCountryList = getStateCountryByCity($sCity);			
		}

		header("Content-Type: application/json");
		echo(json_encode($aStateCountryList));
		break;

	//! add user
	case 'user_registration':
		$sName = isset($_REQUEST['idName']) ? $_REQUEST['idName'] : '';
		$sUserEmail = isset($_REQUEST['idUserEmail']) ? $_REQUEST['idUserEmail'] : '';
		$sUserMobile = isset($_REQUEST['idUserMobile']) ? $_REQUEST['idUserMobile'] : '';
		$sUserCity = isset($_REQUEST['idUserCity']) ? $_REQUEST['idUserCity'] : '';
		$sUserState = isset($_REQUEST['idUserState']) ? $_REQUEST['idUserState'] : '';
		$sUserCountry = isset($_REQUEST['idUserCountry']) ? $_REQUEST['idUserCountry'] : '';
		$sUserName = isset($_REQUEST['idUserName']) ? $_REQUEST['idUserName'] : '';
		$sPassword = isset($_REQUEST['idUserPassword']) ? $_REQUEST['idUserPassword'] : '';
		$iUserTypeID = 3; //! Parent..
		$bResult = false;
		$iUserID = 0;
		$iResult = 0;
		$oUser = new User();
		$oUser->sName = $sName;
		$oUser->sEmail = $sUserEmail;
		$oUser->iMobile = $sUserMobile;
		$oUser->sUsername = $sUserName;
		$oUser->iUserTypeID = $iUserTypeID;

		$iUserID = $oUser->fAddUser($sPassword);
		$oUser->iID = $iUserID;

		if($iUserID > 0){
			$iResult = $oUser->fAddParentDetails($iUserID,0,'',$sUserCity,$sUserState,$sUserCountry,'');
		}

		if($iUserID > 0 && $iResult > 0){
			$bResult = true;
		}

		header("Content-Type: application/json");
		echo(json_encode($bResult));
		break;

	//! child unique number
	case 'child_unique_number':
		
		$sUniqueNumber = fGetChildUniqueNumber();

		header("Content-Type: application/json");
		echo(json_encode($sUniqueNumber));
		break;

	//! child registration
	case 'child_registration':
		
		$aChildUniqueNumber = isset($_REQUEST['idChildUniqueNumber']) ? $_REQUEST['idChildUniqueNumber'] : $_REQUEST['idChildUniqueNumber'];
		$aChildName = isset($_REQUEST['idChildName']) ? $_REQUEST['idChildName'] : $_REQUEST['idChildName'];
		$aChildDOB = isset($_REQUEST['idChildDOB']) ? $_REQUEST['idChildDOB'] : $_REQUEST['idChildDOB'];
		$iUserID = isset($_REQUEST['idUserID']) ? $_REQUEST['idUserID'] : 0;

		$iParentID = fGetParentIDByUserID($iUserID);

		$aResult = array();
		$bResult = true;
		
		if(!empty($aChildUniqueNumber) && $iParentID > 0){
			foreach ($aChildUniqueNumber as $iIndex => $aChildData) {
				$sChildNumber = isset($aChildData) ? $aChildData : '';
				$sChildName = isset($aChildName[$iIndex]) ? $aChildName[$iIndex] : '';
				$dChildDOB = isset($aChildDOB[$iIndex]) ? date('Y-m-d', strtotime($aChildDOB[$iIndex])) : '';

				if($sChildNumber != '' && $dChildDOB != ''){
					$iChildID = fAddChildDetails($iParentID, $sChildNumber, $sChildName, $dChildDOB);

					$aVaccinationMaster = fGetVaccinationMaster();

					if(!empty($aVaccinationMaster)){
						foreach ($aVaccinationMaster as $iIndex => $aVaccination) {
							$iNoOfDays = $aVaccination['no_of_days'];
							$iVaccinationID = $aVaccination['vaccination_id'];
							$dIdealDate = date('Y-m-d', strtotime($aChildDOB[$iIndex]. ' + '.$aVaccination['no_of_days'].' days'));

							//! Add ideal date details..
							fAddIdealDateDetails($iVaccinationID, $iChildID, $iParentID, $dIdealDate);
						}
					}
				}
			}
		}

		if(in_array(false, $aResult)){
			$bResult = false;
		}

		header("Content-Type: application/json");
		echo(json_encode($bResult));
		break;

	//! child registration
	case 'vaccination_group_details':
		//! Fetching vaccination groups..
		$aVaccinationGroupDetails = fGetVaccinationGroupDetails();

		header("Content-Type: application/json");
		echo(json_encode($aVaccinationGroupDetails));
		break;

	//! child registration
	case 'vaccination_list_by_age':
		$iMinAge = isset($_REQUEST['iMinAge']) ? $_REQUEST['iMinAge'] : 0;
		$iMaxAge = isset($_REQUEST['iMaxAge']) ? $_REQUEST['iMaxAge'] : 0;
		$sChildNo = isset($_REQUEST['sChildNo']) ? $_REQUEST['sChildNo'] : '';

		//! Fetching vaccination groups..
		$aVaccinationListing = fGetVaccinationListByAge($iMinAge,$iMaxAge,$sChildNo);

		header("Content-Type: application/json");
		echo(json_encode($aVaccinationListing));
		break;

	//! child registration
	case 'add_vaccine':
		$dReceivedDate = isset($_REQUEST['dReceivedDate']) ? date('Y-m-d', strtotime($_REQUEST['dReceivedDate'])) : '';
		$sRemark = isset($_REQUEST['sRemark']) ? addslashes($_REQUEST['sRemark']) : '';
		$sChildNo = isset($_REQUEST['sChildNo']) ? $_REQUEST['sChildNo'] : '';
		$iUserID = isset($_REQUEST['iUserID']) ? $_REQUEST['iUserID'] : '';
		$iVaccinationID = isset($_REQUEST['iVaccinationID']) ? $_REQUEST['iVaccinationID'] : '';


		$iChildID = fGetChildIDByChildNo($sChildNo);

		$iParentID = fGetParentIDByUserID($iUserID);

		//! Fetching vaccination groups..
		$bResult = fAddReceivedDateDetails($iVaccinationID, $iChildID, $iParentID, $dReceivedDate, $sRemark);

		header("Content-Type: application/json");
		echo(json_encode($bResult));
		break;

	//! add new vaccine
	case 'add_new_vaccine':
		$sVaccineName = isset($_REQUEST['idNewVaccineName']) ? $_REQUEST['idNewVaccineName'] : '';
		$iNoOfDays = isset($_REQUEST['idNoOfDays']) ? $_REQUEST['idNoOfDays'] : '';

		//! Fetching vaccination groups..
		$iVaccinationID = fAddNewVaccine($sVaccineName, $iNoOfDays);

		if($iVaccinationID > 0){
			$aAllChilds = fGetAllChildDetails();

			if(!empty($aAllChilds)){
				foreach ($aAllChilds as $iIndex => $aChild) {
					$dIdealDate = date('Y-m-d', strtotime($aChild['date_of_birth']. ' + '.$iNoOfDays.' days'));

					//! Add ideal date details..
					fAddIdealDateDetails($iVaccinationID, $aChild['child_id'], $aChild['parent_id'], $dIdealDate);
				}
			}
		}

		header("Content-Type: application/json");
		echo(json_encode($bResult));
		break;

	default:
		# code...
		break;
}


?>