<?php

//! It contains the Email template constant variable
//! Include configEhr.php file for constant variable values.
include_once "config/configApp.php";
require_once ABS_PATH_TO_APP."config/config.EmailTemplate.php";
require_once ABS_PATH_TO_APP."config/config.Email_SMS.php";
require_once ABS_PATH_TO_APP."classes/class.Email.php";
require_once ABS_PATH_TO_APP."classes/class.EmailTemplateBuilder.php";

//! brief function to send Email using Email class
//! Parameter : Recipient details, Email template flag, Email info
//! $aRecipientEID - Array - It contains all the recipient numbers
//! $aRecipientName - Array - It contains all the recipient names
//! $iEmailTemplateFlag - Integer - It contains Email template flag value
//! $aEmailInfo - Array - It contains the Email info which is used toe create Email template with dynamic values.
//! $sSenderEID - Array - It contains sender email id and it is not mandatory
//! $sSenderEName - Array - It contains sender name and it is not mandatory
//! Return : TRUE -> Send Email successfully, FALSE -> Not send and gives the error message

function fSendEmail($aRecipientEID,$aRecipientName,$iEmailTemplateFlag,$aEmailInfo,$aAttachement,$sSenderEID,$sSenderEName){

	$iResult = 0;

	if(EMAIL_ON == FALSE){
		return TRUE;
	}

	if(TEST_EMAIL == TRUE){
		$aRecipientEID = array(TEST_EMAIL_ID);
	}

	//! Create Email template
	$aEmailContent = fCreateEmailContent($iEmailTemplateFlag,$aEmailInfo);	

	if(!empty($aEmailContent)){

		$iRecipientCount = count($aRecipientEID);
		$sSubject = $aEmailContent['sSubject'];
		$sBody = $aEmailContent['sBody'];

		$oEmailBuilder = new EmailTemplateBuilder();
		$oEmailBuilder->fCreateOneColumnTemplate($sBody);
		$oEmailBuilder->fClose();
		$sBody = $oEmailBuilder->sEmailTemplate;

		$oEmail = new Email($aRecipientEID[0],$aRecipientName[0],$sSubject,$sBody,$sSenderEID,$sSenderEName);

		//! Check when Email created successfully
		if($oEmail->iInsertEId > 0){

			if($iRecipientCount > 1){

				//! Added Recipient if it is more that one
				for($iii=1;$iii<$iRecipientCount;$iii++){
					$oEmail->addRecipient($aRecipientEID[$iii],$aRecipientName[$iii]);
				}
			}

			//! add attachment id any
			if(!empty($aAttachement)){

				for($kkk=0;$kkk<count($aAttachement);$kkk++){
					$oEmail->addAttachment($aAttachement[$kkk],''); //! Set Filename to null menas it will gose with real file name.
				}
			}
		}

		//! Send Email
		$iResult = $oEmail->sendNow();

	}

	return $iResult;
}

//! brief function to create a Email template
//! Parameter - Email template flag, Email dynamic info
//! Return - string - Email Template for successful result, 0 for unsuccessful
function fCreateEmailContent($iEmailTemplateFlag,$aEmailInfo){

  	$aEmailContent = array();
  	
  	if($iEmailTemplateFlag == SEND_EMAIL_FOR_BCG_VACCINE){

  		$sVaccineName = $aEmailInfo['sVaccineName'];
  		$dVaccineDate = $aEmailInfo['dVaccineDate'];
  		$sParentName = $aEmailInfo['sParentName'];
  		$sChildNumber=  $aEmailInfo['sChildNumber'];
  		$sEventTitle = urlencode($aEmailInfo['sEventTitle']);
  		$sEventLocation = urlencode($aEmailInfo['sEventLocation']);
  		$sEventDetails = urlencode($aEmailInfo['sEventDetails']);
  		$dEventDate = date('Ymd', strtotime($aEmailInfo['dEventDate']));

		$aEmailContent['sSubject'] = "Reminder : Get your child vaccinated for {$sVaccineName} on {$dVaccineDate}";

		$aEmailContent['sBody'] = <<<EOB
			<span style='font-weight: bold;font-size: 18px'> Dear {$sParentName}, </span>
			<br/><br/>
			Your child <b>{$sChildNumber}</b>, is due for Vaccination <b>{$sVaccineName}</b> on <b>{$dVaccineDate}</b>.<br><br>
			Kindly visit your nearby centre.<br><br>
			<b>Benfit:-</b> BCG, or bacille Calmette-Guerin, vaccine protects against Mycobacterium tuberculosis (TB) disease.<br><br>
			<b>It's Good To Know it Early When it Comes To Health!</b>
			<br/><br/>
			
			
			<strong><a href="http://www.google.com/calendar/event?action=TEMPLATE&dates={$dEventDate}/{$dEventDate}&text={$sEventTitle}&location={$sEventLocation}&details={$sEventDetails}" style="display: block;" class="cta gmail-show">Save Event In Calender</a></strong>
			
EOB;

  	}else if($iEmailTemplateFlag == SEND_EMAIL_FOR_HEPATITIS_B_BIRTH_DOSE_VACCINE){

  		$sVaccineName = $aEmailInfo['sVaccineName'];
  		$dVaccineDate = $aEmailInfo['dVaccineDate'];
  		$sParentName = $aEmailInfo['sParentName'];
  		$sEventTitle = urlencode($aEmailInfo['sEventTitle']);
  		$sEventLocation = urlencode($aEmailInfo['sEventLocation']);
  		$sEventDetails = urlencode($aEmailInfo['sEventDetails']);
  		$dEventDate = date('Ymd', strtotime($aEmailInfo['dEventDate']));

		$aEmailContent['sSubject'] = "Reminder : Get your child vaccinated for {$sVaccineName} on {$dVaccineDate}";
  		
				$aEmailContent['sBody'] = <<<EOB
					<span style='font-weight: bold;font-size: 18px'> Dear {$sParentName}, </span>
					<br/><br/>
					Your child <b>{$sChildNumber}</b>, is due for Vaccination <b>{$sVaccineName}</b> on <b>{$dVaccineDate}</b>.<br><br>
					Kindly visit your nearby centre.<br><br>
					<b>Benfit:-</b> It protects against hepatitis B infection and its complications such as permanent liver damage, which can lead to liver cancer and death. When you get immunized, you help protect others as well<br><br>
					<b>It's Good To Know it Early When it Comes To Health!</b>
					<br/><br/>
					
					
					<strong><a href="http://www.google.com/calendar/event?action=TEMPLATE&dates={$dEventDate}/{$dEventDate}&text={$sEventTitle}&location={$sEventLocation}&details={$sEventDetails}" style="display: block;" class="cta gmail-show">Save Event In Calender</a></strong>
					
EOB;

  	}else if($iEmailTemplateFlag == SEND_EMAIL_FOR_OPV_BIRTH_DOSE_VACCINE){

  		$sVaccineName = $aEmailInfo['sVaccineName'];
  		$dVaccineDate = $aEmailInfo['dVaccineDate'];
  		$sParentName = $aEmailInfo['sParentName'];
  		$sEventTitle = urlencode($aEmailInfo['sEventTitle']);
  		$sEventLocation = urlencode($aEmailInfo['sEventLocation']);
  		$sEventDetails = urlencode($aEmailInfo['sEventDetails']);
  		$dEventDate = date('Ymd', strtotime($aEmailInfo['dEventDate']));

		$aEmailContent['sSubject'] = "Reminder : Get your child vaccinated for {$sVaccineName} on {$dVaccineDate}";
  		
		
				
				$aEmailContent['sBody'] = <<<EOB
					<span style='font-weight: bold;font-size: 18px'> Dear {$sParentName}, </span>
					<br/><br/>
					Your child <b>{$sChildNumber}</b>, is due for Vaccination <b>{$sVaccineName}</b> on <b>{$dVaccineDate}</b>.<br><br>
					Kindly visit your nearby centre.<br><br>
					<b>Benfit:-</b> OPV produces antibodies in the blood ('humoral' or serum immunity) to all three types of poliovirus, and in the event of infection, this protects the individual against polio paralysis by preventing the spread of poliovirus to the nervous system.<br><br>
					<b>It's Good To Know it Early When it Comes To Health!</b>
					<br/><br/>
					
					
					<strong><a href="http://www.google.com/calendar/event?action=TEMPLATE&dates={$dEventDate}/{$dEventDate}&text={$sEventTitle}&location={$sEventLocation}&details={$sEventDetails}" style="display: block;" class="cta gmail-show">Save Event In Calender</a></strong>
					
EOB;

  	}else if($iEmailTemplateFlag == SEND_EMAIL_FOR_OPV_1_VACCINE){

  		$sVaccineName = $aEmailInfo['sVaccineName'];
  		$dVaccineDate = $aEmailInfo['dVaccineDate'];
  		$sParentName = $aEmailInfo['sParentName'];
  		$sEventTitle = urlencode($aEmailInfo['sEventTitle']);
  		$sEventLocation = urlencode($aEmailInfo['sEventLocation']);
  		$sEventDetails = urlencode($aEmailInfo['sEventDetails']);
  		$dEventDate = date('Ymd', strtotime($aEmailInfo['dEventDate']));

		$aEmailContent['sSubject'] = "Reminder : Get your child vaccinated for {$sVaccineName} on {$dVaccineDate}";
  		
		$aEmailContent['sBody'] = <<<EOB
					<span style='font-weight: bold;font-size: 18px'> Dear {$sParentName}, </span>
					<br/><br/>
					Your child <b>{$sChildNumber}</b>, is due for Vaccination <b>{$sVaccineName}</b> on <b>{$dVaccineDate}</b>.<br><br>
					Kindly visit your nearby centre.<br><br>
					<b>Benfit:-</b> OPV produces antibodies in the blood ('humoral' or serum immunity) to all three types of poliovirus, and in the event of infection, this protects the individual against polio paralysis by preventing the spread of poliovirus to the nervous system.<br><br>
					<b>It's Good To Know it Early When it Comes To Health!</b>
					<br/><br/>
					
					
					<strong><a href="http://www.google.com/calendar/event?action=TEMPLATE&dates={$dEventDate}/{$dEventDate}&text={$sEventTitle}&location={$sEventLocation}&details={$sEventDetails}" style="display: block;" class="cta gmail-show">Save Event In Calender</a></strong>
					
EOB;

  	}else if($iEmailTemplateFlag == SEND_EMAIL_FOR_OPV_2_VACCINE){

  		$sVaccineName = $aEmailInfo['sVaccineName'];
  		$dVaccineDate = $aEmailInfo['dVaccineDate'];
  		$sParentName = $aEmailInfo['sParentName'];
  		$sEventTitle = urlencode($aEmailInfo['sEventTitle']);
  		$sEventLocation = urlencode($aEmailInfo['sEventLocation']);
  		$sEventDetails = urlencode($aEmailInfo['sEventDetails']);
  		$dEventDate = date('Ymd', strtotime($aEmailInfo['dEventDate']));

		$aEmailContent['sSubject'] = "Reminder : Get your child vaccinated for {$sVaccineName} on {$dVaccineDate}";
  		
		$aEmailContent['sBody'] = <<<EOB
					<span style='font-weight: bold;font-size: 18px'> Dear {$sParentName}, </span>
					<br/><br/>
					Your child <b>{$sChildNumber}</b>, is due for Vaccination <b>{$sVaccineName}</b> on <b>{$dVaccineDate}</b>.<br><br>
					Kindly visit your nearby centre.<br><br>
					<b>Benfit:-</b> OPV produces antibodies in the blood ('humoral' or serum immunity) to all three types of poliovirus, and in the event of infection, this protects the individual against polio paralysis by preventing the spread of poliovirus to the nervous system.<br><br>
					<b>It's Good To Know it Early When it Comes To Health!</b>
					<br/><br/>
					
					
					<strong><a href="http://www.google.com/calendar/event?action=TEMPLATE&dates={$dEventDate}/{$dEventDate}&text={$sEventTitle}&location={$sEventLocation}&details={$sEventDetails}" style="display: block;" class="cta gmail-show">Save Event In Calender</a></strong>
					
EOB;

  	}else if($iEmailTemplateFlag == SEND_EMAIL_FOR_OPV_3_VACCINE){

  		$sVaccineName = $aEmailInfo['sVaccineName'];
  		$dVaccineDate = $aEmailInfo['dVaccineDate'];
  		$sParentName = $aEmailInfo['sParentName'];
  		$sEventTitle = urlencode($aEmailInfo['sEventTitle']);
  		$sEventLocation = urlencode($aEmailInfo['sEventLocation']);
  		$sEventDetails = urlencode($aEmailInfo['sEventDetails']);
  		$dEventDate = date('Ymd', strtotime($aEmailInfo['dEventDate']));

		$aEmailContent['sSubject'] = "Reminder : Get your child vaccinated for {$sVaccineName} on {$dVaccineDate}";
  		
		$aEmailContent['sBody'] = <<<EOB
					<span style='font-weight: bold;font-size: 18px'> Dear {$sParentName}, </span>
					<br/><br/>
					Your child <b>{$sChildNumber}</b>, is due for Vaccination <b>{$sVaccineName}</b> on <b>{$dVaccineDate}</b>.<br><br>
					Kindly visit your nearby centre.<br><br>
					<b>Benfit:-</b> OPV produces antibodies in the blood ('humoral' or serum immunity) to all three types of poliovirus, and in the event of infection, this protects the individual against polio paralysis by preventing the spread of poliovirus to the nervous system.<br><br>
					<b>It's Good To Know it Early When it Comes To Health!</b>
					<br/><br/>
					
					
					<strong><a href="http://www.google.com/calendar/event?action=TEMPLATE&dates={$dEventDate}/{$dEventDate}&text={$sEventTitle}&location={$sEventLocation}&details={$sEventDetails}" style="display: block;" class="cta gmail-show">Save Event In Calender</a></strong>
					
EOB;

  	}else if($iEmailTemplateFlag == SEND_EMAIL_FOR_IPV_VACCINE){

  		$sVaccineName = $aEmailInfo['sVaccineName'];
  		$dVaccineDate = $aEmailInfo['dVaccineDate'];
  		$sParentName = $aEmailInfo['sParentName'];
  		$sEventTitle = urlencode($aEmailInfo['sEventTitle']);
  		$sEventLocation = urlencode($aEmailInfo['sEventLocation']);
  		$sEventDetails = urlencode($aEmailInfo['sEventDetails']);
  		$dEventDate = date('Ymd', strtotime($aEmailInfo['dEventDate']));

		$aEmailContent['sSubject'] = "Reminder : Get your child vaccinated for {$sVaccineName} on {$dVaccineDate}";
  		
		$aEmailContent['sBody'] = <<<EOB
					<span style='font-weight: bold;font-size: 18px'> Dear {$sParentName}, </span>
					<br/><br/>
					Your child <b>{$sChildNumber}</b>, is due for Vaccination <b>{$sVaccineName}</b> on <b>{$dVaccineDate}</b>.<br><br>
					Kindly visit your nearby centre.<br><br>
					<b>Benfit:-</b> IPV will protect against the type 2 poliovirus after the type 2 component of OPV is removed. IPV will also boost immunity to poliovirus types 1 and 3 in children who have previously received OPV, which can contribute to the eradication of these types of polio.<br><br>
					<b>It's Good To Know it Early When it Comes To Health!</b>
					<br/><br/>
					
					
					<strong><a href="http://www.google.com/calendar/event?action=TEMPLATE&dates={$dEventDate}/{$dEventDate}&text={$sEventTitle}&location={$sEventLocation}&details={$sEventDetails}" style="display: block;" class="cta gmail-show">Save Event In Calender</a></strong>
					
EOB;

  	}else if($iEmailTemplateFlag == SEND_EMAIL_FOR_PENTAVELANT_1_VACCINE){

  		$sVaccineName = $aEmailInfo['sVaccineName'];
  		$dVaccineDate = $aEmailInfo['dVaccineDate'];
  		$sParentName = $aEmailInfo['sParentName'];
  		$sEventTitle = urlencode($aEmailInfo['sEventTitle']);
  		$sEventLocation = urlencode($aEmailInfo['sEventLocation']);
  		$sEventDetails = urlencode($aEmailInfo['sEventDetails']);
  		$dEventDate = date('Ymd', strtotime($aEmailInfo['dEventDate']));

		$aEmailContent['sSubject'] = "Reminder : Get your child vaccinated for {$sVaccineName} on {$dVaccineDate}";
  		
		$aEmailContent['sBody'] = <<<EOB
					<span style='font-weight: bold;font-size: 18px'> Dear {$sParentName}, </span>
					<br/><br/>
					Your child <b>{$sChildNumber}</b>, is due for Vaccination <b>{$sVaccineName}</b> on <b>{$dVaccineDate}</b>.<br><br>
					Kindly visit your nearby centre.<br><br>
					<b>Benfit:-</b> BCG, or bacille Calmette-Guerin, vaccine protects against Mycobacterium tuberculosis (TB) disease.<br><br>
					<b>It's Good To Know it Early When it Comes To Health!</b>
					<br/><br/>
					
					
					<strong><a href="http://www.google.com/calendar/event?action=TEMPLATE&dates={$dEventDate}/{$dEventDate}&text={$sEventTitle}&location={$sEventLocation}&details={$sEventDetails}" style="display: block;" class="cta gmail-show">Save Event In Calender</a></strong>
					
EOB;

  	}else if($iEmailTemplateFlag == SEND_EMAIL_FOR_PENTAVELANT_2_VACCINE){

  		$sVaccineName = $aEmailInfo['sVaccineName'];
  		$dVaccineDate = $aEmailInfo['dVaccineDate'];
  		$sParentName = $aEmailInfo['sParentName'];
  		$sEventTitle = urlencode($aEmailInfo['sEventTitle']);
  		$sEventLocation = urlencode($aEmailInfo['sEventLocation']);
  		$sEventDetails = urlencode($aEmailInfo['sEventDetails']);
  		$dEventDate = date('Ymd', strtotime($aEmailInfo['dEventDate']));

		$aEmailContent['sSubject'] = "Reminder : Get your child vaccinated for {$sVaccineName} on {$dVaccineDate}";
  		
		$aEmailContent['sBody'] = <<<EOB
					<span style='font-weight: bold;font-size: 18px'> Dear {$sParentName}, </span>
					<br/><br/>
					Your child <b>{$sChildNumber}</b>, is due for Vaccination <b>{$sVaccineName}</b> on <b>{$dVaccineDate}</b>.<br><br>
					Kindly visit your nearby centre.<br><br>
					<b>Benfit:-</b> BCG, or bacille Calmette-Guerin, vaccine protects against Mycobacterium tuberculosis (TB) disease.<br><br>
					<b>It's Good To Know it Early When it Comes To Health!</b>
					<br/><br/>
					
					
					<strong><a href="http://www.google.com/calendar/event?action=TEMPLATE&dates={$dEventDate}/{$dEventDate}&text={$sEventTitle}&location={$sEventLocation}&details={$sEventDetails}" style="display: block;" class="cta gmail-show">Save Event In Calender</a></strong>
					
EOB;

  	}else if($iEmailTemplateFlag == SEND_EMAIL_FOR_PENTAVELANT_3_VACCINE){

  		$sVaccineName = $aEmailInfo['sVaccineName'];
  		$dVaccineDate = $aEmailInfo['dVaccineDate'];
  		$sParentName = $aEmailInfo['sParentName'];
  		$sEventTitle = urlencode($aEmailInfo['sEventTitle']);
  		$sEventLocation = urlencode($aEmailInfo['sEventLocation']);
  		$sEventDetails = urlencode($aEmailInfo['sEventDetails']);
  		$dEventDate = date('Ymd', strtotime($aEmailInfo['dEventDate']));

		$aEmailContent['sSubject'] = "Reminder : Get your child vaccinated for {$sVaccineName} on {$dVaccineDate}";
  		
		$aEmailContent['sBody'] = <<<EOB
					<span style='font-weight: bold;font-size: 18px'> Dear {$sParentName}, </span>
					<br/><br/>
					Your child <b>{$sChildNumber}</b>, is due for Vaccination <b>{$sVaccineName}</b> on <b>{$dVaccineDate}</b>.<br><br>
					Kindly visit your nearby centre.<br><br>
					<b>Benfit:-</b>  BCG, or bacille Calmette-Guerin, vaccine protects against Mycobacterium tuberculosis (TB) disease.<br><br>
					<b>It's Good To Know it Early When it Comes To Health!</b>
					<br/><br/>
					
					
					<strong><a href="http://www.google.com/calendar/event?action=TEMPLATE&dates={$dEventDate}/{$dEventDate}&text={$sEventTitle}&location={$sEventLocation}&details={$sEventDetails}" style="display: block;" class="cta gmail-show">Save Event In Calender</a></strong>
					
EOB;

  	}else if($iEmailTemplateFlag == SEND_EMAIL_FOR_ROTAVIRUS_1_VACCINE){

  		$sVaccineName = $aEmailInfo['sVaccineName'];
  		$dVaccineDate = $aEmailInfo['dVaccineDate'];
  		$sParentName = $aEmailInfo['sParentName'];
  		$sEventTitle = urlencode($aEmailInfo['sEventTitle']);
  		$sEventLocation = urlencode($aEmailInfo['sEventLocation']);
  		$sEventDetails = urlencode($aEmailInfo['sEventDetails']);
  		$dEventDate = date('Ymd', strtotime($aEmailInfo['dEventDate']));

		$aEmailContent['sSubject'] = "Reminder : Get your child vaccinated for {$sVaccineName} on {$dVaccineDate}";
  		
		$aEmailContent['sBody'] = <<<EOB
					<span style='font-weight: bold;font-size: 18px'> Dear {$sParentName}, </span>
					<br/><br/>
					Your child <b>{$sChildNumber}</b>, is due for Vaccination <b>{$sVaccineName}</b> on <b>{$dVaccineDate}</b>.<br><br>
					Kindly visit your nearby centre.<br><br>
					<b>Benfit:-</b>  The rotavirus vaccine used in the UK gives protection against type A rotavirus infections that cause vomiting and severe diarrhoea in infants and children. Their use has resulted in reductions of infant diarrheal deaths, hospitalizations and incidence of rotavirus gastroenteritis.The rotavirus vaccine protects 9 out of 10 children from getting severe illness caused by rotavirus.<br><br>
					<b>It's Good To Know it Early When it Comes To Health!</b>
					<br/><br/>
					
					
					<strong><a href="http://www.google.com/calendar/event?action=TEMPLATE&dates={$dEventDate}/{$dEventDate}&text={$sEventTitle}&location={$sEventLocation}&details={$sEventDetails}" style="display: block;" class="cta gmail-show">Save Event In Calender</a></strong>
					
EOB;

  	}else if($iEmailTemplateFlag == SEND_EMAIL_FOR_ROTAVIRUS_2_VACCINE){

  		$sVaccineName = $aEmailInfo['sVaccineName'];
  		$dVaccineDate = $aEmailInfo['dVaccineDate'];
  		$sParentName = $aEmailInfo['sParentName'];
  		$sEventTitle = urlencode($aEmailInfo['sEventTitle']);
  		$sEventLocation = urlencode($aEmailInfo['sEventLocation']);
  		$sEventDetails = urlencode($aEmailInfo['sEventDetails']);
  		$dEventDate = date('Ymd', strtotime($aEmailInfo['dEventDate']));

		$aEmailContent['sSubject'] = "Reminder : Get your child vaccinated for {$sVaccineName} on {$dVaccineDate}";
  		
		$aEmailContent['sBody'] = <<<EOB
					<span style='font-weight: bold;font-size: 18px'> Dear {$sParentName}, </span>
					<br/><br/>
					Your child <b>{$sChildNumber}</b>, is due for Vaccination <b>{$sVaccineName}</b> on <b>{$dVaccineDate}</b>.<br><br>
					Kindly visit your nearby centre.<br><br>
					<b>Benfit:-</b>  The rotavirus vaccine used in the UK gives protection against type A rotavirus infections that cause vomiting and severe diarrhoea in infants and children.Their use has resulted in reductions of infant diarrheal deaths, hospitalizations and incidence of rotavirus gastroenteritis.The rotavirus vaccine protects 9 out of 10 children from getting severe illness caused by rotavirus.<br><br>
					<b>It's Good To Know it Early When it Comes To Health!</b>
					<br/><br/>
					
					
					<strong><a href="http://www.google.com/calendar/event?action=TEMPLATE&dates={$dEventDate}/{$dEventDate}&text={$sEventTitle}&location={$sEventLocation}&details={$sEventDetails}" style="display: block;" class="cta gmail-show">Save Event In Calender</a></strong>
					
EOB;

  	}else if($iEmailTemplateFlag == SEND_EMAIL_FOR_ROTAVIRUS_3_VACCINE){

  		$sVaccineName = $aEmailInfo['sVaccineName'];
  		$dVaccineDate = $aEmailInfo['dVaccineDate'];
  		$sParentName = $aEmailInfo['sParentName'];
  		$sEventTitle = urlencode($aEmailInfo['sEventTitle']);
  		$sEventLocation = urlencode($aEmailInfo['sEventLocation']);
  		$sEventDetails = urlencode($aEmailInfo['sEventDetails']);
  		$dEventDate = date('Ymd', strtotime($aEmailInfo['dEventDate']));

		$aEmailContent['sSubject'] = "Reminder : Get your child vaccinated for {$sVaccineName} on {$dVaccineDate}";
  		
		$aEmailContent['sBody'] = <<<EOB
					<span style='font-weight: bold;font-size: 18px'> Dear {$sParentName}, </span>
					<br/><br/>
					Your child <b>{$sChildNumber}</b>, is due for Vaccination <b>{$sVaccineName}</b> on <b>{$dVaccineDate}</b>.<br><br>
					Kindly visit your nearby centre.<br><br>
					<b>Benfit:-</b>  The rotavirus vaccine used in the UK gives protection against type A rotavirus infections that cause vomiting and severe diarrhoea in infants and children.Their use has resulted in reductions of infant diarrheal deaths, hospitalizations and incidence of rotavirus gastroenteritis.The rotavirus vaccine protects 9 out of 10 children from getting severe illness caused by rotavirus.<br><br>
					<b>It's Good To Know it Early When it Comes To Health!</b>
					<br/><br/>
					
					
					<strong><a href="http://www.google.com/calendar/event?action=TEMPLATE&dates={$dEventDate}/{$dEventDate}&text={$sEventTitle}&location={$sEventLocation}&details={$sEventDetails}" style="display: block;" class="cta gmail-show">Save Event In Calender</a></strong>
					
EOB;

  	}else if($iEmailTemplateFlag == SEND_EMAIL_FOR_MEASLES_1_VACCINE){

  		$sVaccineName = $aEmailInfo['sVaccineName'];
  		$dVaccineDate = $aEmailInfo['dVaccineDate'];
  		$sParentName = $aEmailInfo['sParentName'];
  		$sEventTitle = urlencode($aEmailInfo['sEventTitle']);
  		$sEventLocation = urlencode($aEmailInfo['sEventLocation']);
  		$sEventDetails = urlencode($aEmailInfo['sEventDetails']);
  		$dEventDate = date('Ymd', strtotime($aEmailInfo['dEventDate']));

		$aEmailContent['sSubject'] = "Reminder : Get your child vaccinated for {$sVaccineName} on {$dVaccineDate}";
  		
		$aEmailContent['sBody'] = <<<EOB
					<span style='font-weight: bold;font-size: 18px'> Dear {$sParentName}, </span>
					<br/><br/>
					Your child <b>{$sChildNumber}</b>, is due for Vaccination <b>{$sVaccineName}</b> on <b>{$dVaccineDate}</b>.<br><br>
					Kindly visit your nearby centre.<br><br>
					<b>Benfit:-</b> MMR vaccine is very effective at protecting people against measles, mumps, and rubella, and preventing the complications caused by these diseases. People who received two doses of MMR vaccine as children according to the U.S. vaccination schedule are considered protected for life.<br><br>
					<b>It's Good To Know it Early When it Comes To Health!</b>
					<br/><br/>
					
					
					<strong><a href="http://www.google.com/calendar/event?action=TEMPLATE&dates={$dEventDate}/{$dEventDate}&text={$sEventTitle}&location={$sEventLocation}&details={$sEventDetails}" style="display: block;" class="cta gmail-show">Save Event In Calender</a></strong>
					
EOB;

  	}else if($iEmailTemplateFlag == SEND_EMAIL_FOR_VITAMIN_A_1_VACCINE){

  		$sVaccineName = $aEmailInfo['sVaccineName'];
  		$dVaccineDate = $aEmailInfo['dVaccineDate'];
  		$sParentName = $aEmailInfo['sParentName'];
  		$sEventTitle = urlencode($aEmailInfo['sEventTitle']);
  		$sEventLocation = urlencode($aEmailInfo['sEventLocation']);
  		$sEventDetails = urlencode($aEmailInfo['sEventDetails']);
  		$dEventDate = date('Ymd', strtotime($aEmailInfo['dEventDate']));

		$aEmailContent['sSubject'] = "Reminder : Get your child vaccinated for {$sVaccineName} on {$dVaccineDate}";
  		
		$aEmailContent['sBody'] = <<<EOB
					<span style='font-weight: bold;font-size: 18px'> Dear {$sParentName}, </span>
					<br/><br/>
					Your child <b>{$sChildNumber}</b>, is due for Vaccination <b>{$sVaccineName}</b> on <b>{$dVaccineDate}</b>.<br><br>
					Kindly visit your nearby centre.<br><br>
					<b>Benfit:-</b> Vitamin A is essential for the functioning of the immune system and the healthy growth and development of children, and is usually acquired through a healthy diet.<br><br>
					<b>It's Good To Know it Early When it Comes To Health!</b>
					<br/><br/>
					
					
					<strong><a href="http://www.google.com/calendar/event?action=TEMPLATE&dates={$dEventDate}/{$dEventDate}&text={$sEventTitle}&location={$sEventLocation}&details={$sEventDetails}" style="display: block;" class="cta gmail-show">Save Event In Calender</a></strong>
					
EOB;

  	}else if($iEmailTemplateFlag == SEND_EMAIL_FOR_DPT_1_VACCINE){

  		$sVaccineName = $aEmailInfo['sVaccineName'];
  		$dVaccineDate = $aEmailInfo['dVaccineDate'];
  		$sParentName = $aEmailInfo['sParentName'];
  		$sEventTitle = urlencode($aEmailInfo['sEventTitle']);
  		$sEventLocation = urlencode($aEmailInfo['sEventLocation']);
  		$sEventDetails = urlencode($aEmailInfo['sEventDetails']);
  		$dEventDate = date('Ymd', strtotime($aEmailInfo['dEventDate']));

		$aEmailContent['sSubject'] = "Reminder : Get your child vaccinated for {$sVaccineName} on {$dVaccineDate}";
  		
		$aEmailContent['sBody'] = <<<EOB
					<span style='font-weight: bold;font-size: 18px'> Dear {$sParentName}, </span>
					<br/><br/>
					Your child <b>{$sChildNumber}</b>, is due for Vaccination <b>{$sVaccineName}</b> on <b>{$dVaccineDate}</b>.<br><br>
					Kindly visit your nearby centre.<br><br>
					<b>Benfit:-</b> Pentavalent Vaccine (DPT-HepB-Hib):Hib vaccine can prevent serious diseases caused by Haemophilus influenzae type b like pneumonia, meningitis, bacteremia, epiglottitis, septic arthritis etc. Giving pentavalent vaccine reduces the number of pricks to a child, and provides protection from all five diseases.<br><br>
					<b>It's Good To Know it Early When it Comes To Health!</b>
					<br/><br/>
					
					
					<strong><a href="http://www.google.com/calendar/event?action=TEMPLATE&dates={$dEventDate}/{$dEventDate}&text={$sEventTitle}&location={$sEventLocation}&details={$sEventDetails}" style="display: block;" class="cta gmail-show">Save Event In Calender</a></strong>
					
EOB;

  	}else if($iEmailTemplateFlag == SEND_EMAIL_FOR_OPV_4_VACCINE){

  		$sVaccineName = $aEmailInfo['sVaccineName'];
  		$dVaccineDate = $aEmailInfo['dVaccineDate'];
  		$sParentName = $aEmailInfo['sParentName'];
  		$sEventTitle = urlencode($aEmailInfo['sEventTitle']);
  		$sEventLocation = urlencode($aEmailInfo['sEventLocation']);
  		$sEventDetails = urlencode($aEmailInfo['sEventDetails']);
  		$dEventDate = date('Ymd', strtotime($aEmailInfo['dEventDate']));

		$aEmailContent['sSubject'] = "Reminder : Get your child vaccinated for {$sVaccineName} on {$dVaccineDate}";
  		
		$aEmailContent['sBody'] = <<<EOB
					<span style='font-weight: bold;font-size: 18px'> Dear {$sParentName}, </span>
					<br/><br/>
					Your child <b>{$sChildNumber}</b>, is due for Vaccination <b>{$sVaccineName}</b> on <b>{$dVaccineDate}</b>.<br><br>
					Kindly visit your nearby centre.<br><br>
					<b>Benfit:-</b> BCG, or bacille Calmette-Guerin, vaccine protects against Mycobacterium tuberculosis (TB) disease.<br><br>
					<b>It's Good To Know it Early When it Comes To Health!</b>
					<br/><br/>
					
					
					<strong><a href="http://www.google.com/calendar/event?action=TEMPLATE&dates={$dEventDate}/{$dEventDate}&text={$sEventTitle}&location={$sEventLocation}&details={$sEventDetails}" style="display: block;" class="cta gmail-show">Save Event In Calender</a></strong>
					
EOB;

  	}else if($iEmailTemplateFlag == SEND_EMAIL_FOR_MEASLES_2_VACCINE){

  		$sVaccineName = $aEmailInfo['sVaccineName'];
  		$dVaccineDate = $aEmailInfo['dVaccineDate'];
  		$sParentName = $aEmailInfo['sParentName'];
  		$sEventTitle = urlencode($aEmailInfo['sEventTitle']);
  		$sEventLocation = urlencode($aEmailInfo['sEventLocation']);
  		$sEventDetails = urlencode($aEmailInfo['sEventDetails']);
  		$dEventDate = date('Ymd', strtotime($aEmailInfo['dEventDate']));

		$aEmailContent['sSubject'] = "Reminder : Get your child vaccinated for {$sVaccineName} on {$dVaccineDate}";
  		
		$aEmailContent['sBody'] = <<<EOB
					<span style='font-weight: bold;font-size: 18px'> Dear {$sParentName}, </span>
					<br/><br/>
					Your child <b>{$sChildNumber}</b>, is due for Vaccination <b>{$sVaccineName}</b> on <b>{$dVaccineDate}</b>.<br><br>
					Kindly visit your nearby centre.<br><br>
					<b>Benfit:-</b> BCG, or bacille Calmette-Guerin, vaccine protects against Mycobacterium tuberculosis (TB) disease.<br><br>
					<b>It's Good To Know it Early When it Comes To Health!</b>
					<br/><br/>
					
					
					<strong><a href="http://www.google.com/calendar/event?action=TEMPLATE&dates={$dEventDate}/{$dEventDate}&text={$sEventTitle}&location={$sEventLocation}&details={$sEventDetails}" style="display: block;" class="cta gmail-show">Save Event In Calender</a></strong>
					
EOB;

  	}else if($iEmailTemplateFlag == SEND_EMAIL_FOR_VITAMIN_A_1_VACCINE){

  		$sVaccineName = $aEmailInfo['sVaccineName'];
  		$dVaccineDate = $aEmailInfo['dVaccineDate'];
  		$sParentName = $aEmailInfo['sParentName'];
  		$sEventTitle = urlencode($aEmailInfo['sEventTitle']);
  		$sEventLocation = urlencode($aEmailInfo['sEventLocation']);
  		$sEventDetails = urlencode($aEmailInfo['sEventDetails']);
  		$dEventDate = date('Ymd', strtotime($aEmailInfo['dEventDate']));

		$aEmailContent['sSubject'] = "Reminder : Get your child vaccinated for {$sVaccineName} on {$dVaccineDate}";
  		
		$aEmailContent['sBody'] = <<<EOB
					<span style='font-weight: bold;font-size: 18px'> Dear {$sParentName}, </span>
					<br/><br/>
					Your child <b>{$sChildNumber}</b>, is due for Vaccination <b>{$sVaccineName}</b> on <b>{$dVaccineDate}</b>.<br><br>
					Kindly visit your nearby centre.<br><br>
					<b>Benfit:-</b> BCG, or bacille Calmette-Guerin, vaccine protects against Mycobacterium tuberculosis (TB) disease.<br><br>
					<b>It's Good To Know it Early When it Comes To Health!</b>
					<br/><br/>
					
					
					<strong><a href="http://www.google.com/calendar/event?action=TEMPLATE&dates={$dEventDate}/{$dEventDate}&text={$sEventTitle}&location={$sEventLocation}&details={$sEventDetails}" style="display: block;" class="cta gmail-show">Save Event In Calender</a></strong>
					
EOB;

  	}else if($iEmailTemplateFlag == SEND_EMAIL_FOR_VITAMIN_A_2_VACCINE){

  		$sVaccineName = $aEmailInfo['sVaccineName'];
  		$dVaccineDate = $aEmailInfo['dVaccineDate'];
  		$sParentName = $aEmailInfo['sParentName'];
  		$sEventTitle = urlencode($aEmailInfo['sEventTitle']);
  		$sEventLocation = urlencode($aEmailInfo['sEventLocation']);
  		$sEventDetails = urlencode($aEmailInfo['sEventDetails']);
  		$dEventDate = date('Ymd', strtotime($aEmailInfo['dEventDate']));

		$aEmailContent['sSubject'] = "Reminder : Get your child vaccinated for {$sVaccineName} on {$dVaccineDate}";
  		
		$aEmailContent['sBody'] = <<<EOB
					<span style='font-weight: bold;font-size: 18px'> Dear {$sParentName}, </span>
					<br/><br/>
					Your child <b>{$sChildNumber}</b>, is due for Vaccination <b>{$sVaccineName}</b> on <b>{$dVaccineDate}</b>.<br><br>
					Kindly visit your nearby centre.<br><br>
					<b>Benfit:-</b> BCG, or bacille Calmette-Guerin, vaccine protects against Mycobacterium tuberculosis (TB) disease.<br><br>
					<b>It's Good To Know it Early When it Comes To Health!</b>
					<br/><br/>
					
					
					<strong><a href="http://www.google.com/calendar/event?action=TEMPLATE&dates={$dEventDate}/{$dEventDate}&text={$sEventTitle}&location={$sEventLocation}&details={$sEventDetails}" style="display: block;" class="cta gmail-show">Save Event In Calender</a></strong>
					
EOB;

  	}else if($iEmailTemplateFlag == SEND_EMAIL_FOR_VITAMIN_A_3_VACCINE){

  		$sVaccineName = $aEmailInfo['sVaccineName'];
  		$dVaccineDate = $aEmailInfo['dVaccineDate'];
  		$sParentName = $aEmailInfo['sParentName'];
  		$sEventTitle = urlencode($aEmailInfo['sEventTitle']);
  		$sEventLocation = urlencode($aEmailInfo['sEventLocation']);
  		$sEventDetails = urlencode($aEmailInfo['sEventDetails']);
  		$dEventDate = date('Ymd', strtotime($aEmailInfo['dEventDate']));

		$aEmailContent['sSubject'] = "Reminder : Get your child vaccinated for {$sVaccineName} on {$dVaccineDate}";
  		
		$aEmailContent['sBody'] = <<<EOB
					<span style='font-weight: bold;font-size: 18px'> Dear {$sParentName}, </span>
					<br/><br/>
					Your child <b>{$sChildNumber}</b>, is due for Vaccination <b>{$sVaccineName}</b> on <b>{$dVaccineDate}</b>.<br><br>
					Kindly visit your nearby centre.<br><br>
					<b>Benfit:-</b> BCG, or bacille Calmette-Guerin, vaccine protects against Mycobacterium tuberculosis (TB) disease.<br><br>
					<b>It's Good To Know it Early When it Comes To Health!</b>
					<br/><br/>
					
					
					<strong><a href="http://www.google.com/calendar/event?action=TEMPLATE&dates={$dEventDate}/{$dEventDate}&text={$sEventTitle}&location={$sEventLocation}&details={$sEventDetails}" style="display: block;" class="cta gmail-show">Save Event In Calender</a></strong>
					
EOB;

  	}else if($iEmailTemplateFlag == SEND_EMAIL_FOR_VITAMIN_A_4_VACCINE){

  		$sVaccineName = $aEmailInfo['sVaccineName'];
  		$dVaccineDate = $aEmailInfo['dVaccineDate'];
  		$sParentName = $aEmailInfo['sParentName'];
  		$sEventTitle = urlencode($aEmailInfo['sEventTitle']);
  		$sEventLocation = urlencode($aEmailInfo['sEventLocation']);
  		$sEventDetails = urlencode($aEmailInfo['sEventDetails']);
  		$dEventDate = date('Ymd', strtotime($aEmailInfo['dEventDate']));

		$aEmailContent['sSubject'] = "Reminder : Get your child vaccinated for {$sVaccineName} on {$dVaccineDate}";
  		
		$aEmailContent['sBody'] = <<<EOB
					<span style='font-weight: bold;font-size: 18px'> Dear {$sParentName}, </span>
					<br/><br/>
					Your child <b>{$sChildNumber}</b>, is due for Vaccination <b>{$sVaccineName}</b> on <b>{$dVaccineDate}</b>.<br><br>
					Kindly visit your nearby centre.<br><br>
					<b>Benfit:-</b> BCG, or bacille Calmette-Guerin, vaccine protects against Mycobacterium tuberculosis (TB) disease.<br><br>
					<b>It's Good To Know it Early When it Comes To Health!</b>
					<br/><br/>
					
					
					<strong><a href="http://www.google.com/calendar/event?action=TEMPLATE&dates={$dEventDate}/{$dEventDate}&text={$sEventTitle}&location={$sEventLocation}&details={$sEventDetails}" style="display: block;" class="cta gmail-show">Save Event In Calender</a></strong>
					
EOB;

  	}else if($iEmailTemplateFlag == SEND_EMAIL_FOR_VITAMIN_A_5_VACCINE){

  		$sVaccineName = $aEmailInfo['sVaccineName'];
  		$dVaccineDate = $aEmailInfo['dVaccineDate'];
  		$sParentName = $aEmailInfo['sParentName'];
  		$sEventTitle = urlencode($aEmailInfo['sEventTitle']);
  		$sEventLocation = urlencode($aEmailInfo['sEventLocation']);
  		$sEventDetails = urlencode($aEmailInfo['sEventDetails']);
  		$dEventDate = date('Ymd', strtotime($aEmailInfo['dEventDate']));

		$aEmailContent['sSubject'] = "Reminder : Get your child vaccinated for {$sVaccineName} on {$dVaccineDate}";
  		
		$aEmailContent['sBody'] = <<<EOB
					<span style='font-weight: bold;font-size: 18px'> Dear {$sParentName}, </span>
					<br/><br/>
					Your child <b>{$sChildNumber}</b>, is due for Vaccination <b>{$sVaccineName}</b> on <b>{$dVaccineDate}</b>.<br><br>
					Kindly visit your nearby centre.<br><br>
					<b>Benfit:-</b> BCG, or bacille Calmette-Guerin, vaccine protects against Mycobacterium tuberculosis (TB) disease.<br><br>
					<b>It's Good To Know it Early When it Comes To Health!</b>
					<br/><br/>
					
					
					<strong><a href="http://www.google.com/calendar/event?action=TEMPLATE&dates={$dEventDate}/{$dEventDate}&text={$sEventTitle}&location={$sEventLocation}&details={$sEventDetails}" style="display: block;" class="cta gmail-show">Save Event In Calender</a></strong>
					
EOB;

  	}else if($iEmailTemplateFlag == SEND_EMAIL_FOR_VITAMIN_A_6_VACCINE){

  		$sVaccineName = $aEmailInfo['sVaccineName'];
  		$dVaccineDate = $aEmailInfo['dVaccineDate'];
  		$sParentName = $aEmailInfo['sParentName'];
  		$sEventTitle = urlencode($aEmailInfo['sEventTitle']);
  		$sEventLocation = urlencode($aEmailInfo['sEventLocation']);
  		$sEventDetails = urlencode($aEmailInfo['sEventDetails']);
  		$dEventDate = date('Ymd', strtotime($aEmailInfo['dEventDate']));

		$aEmailContent['sSubject'] = "Reminder : Get your child vaccinated for {$sVaccineName} on {$dVaccineDate}";
  		
		$aEmailContent['sBody'] = <<<EOB
					<span style='font-weight: bold;font-size: 18px'> Dear {$sParentName}, </span>
					<br/><br/>
					Your child <b>{$sChildNumber}</b>, is due for Vaccination <b>{$sVaccineName}</b> on <b>{$dVaccineDate}</b>.<br><br>
					Kindly visit your nearby centre.<br><br>
					<b>Benfit:-</b> BCG, or bacille Calmette-Guerin, vaccine protects against Mycobacterium tuberculosis (TB) disease.<br><br>
					<b>It's Good To Know it Early When it Comes To Health!</b>
					<br/><br/>
					
					
					<strong><a href="http://www.google.com/calendar/event?action=TEMPLATE&dates={$dEventDate}/{$dEventDate}&text={$sEventTitle}&location={$sEventLocation}&details={$sEventDetails}" style="display: block;" class="cta gmail-show">Save Event In Calender</a></strong>
					
EOB;

  	}else if($iEmailTemplateFlag == SEND_EMAIL_FOR_VITAMIN_A_7_VACCINE){

  		$sVaccineName = $aEmailInfo['sVaccineName'];
  		$dVaccineDate = $aEmailInfo['dVaccineDate'];
  		$sParentName = $aEmailInfo['sParentName'];
  		$sEventTitle = urlencode($aEmailInfo['sEventTitle']);
  		$sEventLocation = urlencode($aEmailInfo['sEventLocation']);
  		$sEventDetails = urlencode($aEmailInfo['sEventDetails']);
  		$dEventDate = date('Ymd', strtotime($aEmailInfo['dEventDate']));

		$aEmailContent['sSubject'] = "Reminder : Get your child vaccinated for {$sVaccineName} on {$dVaccineDate}";
  		
		$aEmailContent['sBody'] = <<<EOB
					<span style='font-weight: bold;font-size: 18px'> Dear {$sParentName}, </span>
					<br/><br/>
					Your child <b>{$sChildNumber}</b>, is due for Vaccination <b>{$sVaccineName}</b> on <b>{$dVaccineDate}</b>.<br><br>
					Kindly visit your nearby centre.<br><br>
					<b>Benfit:-</b> BCG, or bacille Calmette-Guerin, vaccine protects against Mycobacterium tuberculosis (TB) disease.<br><br>
					<b>It's Good To Know it Early When it Comes To Health!</b>
					<br/><br/>
					
					
					<strong><a href="http://www.google.com/calendar/event?action=TEMPLATE&dates={$dEventDate}/{$dEventDate}&text={$sEventTitle}&location={$sEventLocation}&details={$sEventDetails}" style="display: block;" class="cta gmail-show">Save Event In Calender</a></strong>
					
EOB;

  	}else if($iEmailTemplateFlag == SEND_EMAIL_FOR_VITAMIN_A_8_VACCINE){

  		$sVaccineName = $aEmailInfo['sVaccineName'];
  		$dVaccineDate = $aEmailInfo['dVaccineDate'];
  		$sParentName = $aEmailInfo['sParentName'];
  		$sEventTitle = urlencode($aEmailInfo['sEventTitle']);
  		$sEventLocation = urlencode($aEmailInfo['sEventLocation']);
  		$sEventDetails = urlencode($aEmailInfo['sEventDetails']);
  		$dEventDate = date('Ymd', strtotime($aEmailInfo['dEventDate']));

		$aEmailContent['sSubject'] = "Reminder : Get your child vaccinated for {$sVaccineName} on {$dVaccineDate}";
  		
		$aEmailContent['sBody'] = <<<EOB
					<span style='font-weight: bold;font-size: 18px'> Dear {$sParentName}, </span>
					<br/><br/>
					Your child <b>{$sChildNumber}</b>, is due for Vaccination <b>{$sVaccineName}</b> on <b>{$dVaccineDate}</b>.<br><br>
					Kindly visit your nearby centre.<br><br>
					<b>Benfit:-</b> BCG, or bacille Calmette-Guerin, vaccine protects against Mycobacterium tuberculosis (TB) disease.<br><br>
					<b>It's Good To Know it Early When it Comes To Health!</b>
					<br/><br/>
					
					
					<strong><a href="http://www.google.com/calendar/event?action=TEMPLATE&dates={$dEventDate}/{$dEventDate}&text={$sEventTitle}&location={$sEventLocation}&details={$sEventDetails}" style="display: block;" class="cta gmail-show">Save Event In Calender</a></strong>
					
EOB;

  	}else if($iEmailTemplateFlag == SEND_EMAIL_FOR_DPT_2_VACCINE){

  		$sVaccineName = $aEmailInfo['sVaccineName'];
  		$dVaccineDate = $aEmailInfo['dVaccineDate'];
  		$sParentName = $aEmailInfo['sParentName'];
  		$sEventTitle = urlencode($aEmailInfo['sEventTitle']);
  		$sEventLocation = urlencode($aEmailInfo['sEventLocation']);
  		$sEventDetails = urlencode($aEmailInfo['sEventDetails']);
  		$dEventDate = date('Ymd', strtotime($aEmailInfo['dEventDate']));

		$aEmailContent['sSubject'] = "Reminder : Get your child vaccinated for {$sVaccineName} on {$dVaccineDate}";
  		
		$aEmailContent['sBody'] = <<<EOB
					<span style='font-weight: bold;font-size: 18px'> Dear {$sParentName}, </span>
					<br/><br/>
					Your child <b>{$sChildNumber}</b>, is due for Vaccination <b>{$sVaccineName}</b> on <b>{$dVaccineDate}</b>.<br><br>
					Kindly visit your nearby centre.<br><br>
					<b>Benfit:-</b> BCG, or bacille Calmette-Guerin, vaccine protects against Mycobacterium tuberculosis (TB) disease.<br><br>
					<b>It's Good To Know it Early When it Comes To Health!</b>
					<br/><br/>
					
					
					<strong><a href="http://www.google.com/calendar/event?action=TEMPLATE&dates={$dEventDate}/{$dEventDate}&text={$sEventTitle}&location={$sEventLocation}&details={$sEventDetails}" style="display: block;" class="cta gmail-show">Save Event In Calender</a></strong>
					
EOB;

  	}else{
  		$sVaccineName = $aEmailInfo['sVaccineName'];
  		$dVaccineDate = $aEmailInfo['dVaccineDate'];
  		$sParentName = $aEmailInfo['sParentName'];
  		$sEventTitle = urlencode($aEmailInfo['sEventTitle']);
  		$sEventLocation = urlencode($aEmailInfo['sEventLocation']);
  		$sEventDetails = urlencode($aEmailInfo['sEventDetails']);
  		$dEventDate = date('Ymd', strtotime($aEmailInfo['dEventDate']));

		$aEmailContent['sSubject'] = "Reminder : Get your child vaccinated for {$sVaccineName} on {$dVaccineDate}";
  		
		$aEmailContent['sBody'] = <<<EOB
							<span style='font-weight: bold;font-size: 18px'> Dear {$sParentName}, </span>
							<br/><br/>
							Your child <b>{$sChildNumber}</b>, is due for Vaccination <b>{$sVaccineName}</b> on <b>{$dVaccineDate}</b>.<br><br>
							Kindly visit your nearby centre.<br><br>
							<b>Benfit:-</b> vaccine protects against disease.<br><br>
							<b>It's Good To Know it Early When it Comes To Health!</b>
							<br/><br/>
							
							
							<strong><a href="http://www.google.com/calendar/event?action=TEMPLATE&dates={$dEventDate}/{$dEventDate}&text={$sEventTitle}&location={$sEventLocation}&details={$sEventDetails}" style="display: block;" class="cta gmail-show">Save Event In Calender</a></strong>
							
EOB;

  	}

  	return $aEmailContent;
}
?>