<?php
require_once __DIR__.'/../../vendor/autoload.php';

$app = new Slim\App();

$app->add(new Tuupola\Middleware\JwtAuthentication([
    "secret" => CUSTOMER_PORTAL_API_SECRET_KEY,
    "error" => function ($response, $arguments) {
        $data["status"] = "error";
        $data["message"] = $arguments["message"];
        return $response
            ->withHeader("Content-Type", "application/json")
            ->getBody()->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    }
]));


$app->run();
?>