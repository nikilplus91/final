<?php

include_once dirname(__FILE__)."/config/configApp.php";
include_once dirname(__FILE__)."/classes/class.DBConnManager.php";
include_once dirname(__FILE__)."/lib/HTMLPurifier/HTMLPurifier.auto.php";

//! function returns the base64_encode of the @a $string that can be passed in URL
//! @return string encoded string
function urlsafe_b64encode($string)
{
  $data = base64_encode($string);
  $data = str_replace(array('+','/','='),array('-','_','.'),$data);
  return $data;
}

//! function returns the base64_decode of the @a $string that was encoded with urlsafe_b64encode
//! @return string decoded string
function urlsafe_b64decode($string)
{
  $data = str_replace(array('-','_','.'),array('+','/','='),$string);
  $mod4 = strlen($data) % 4;
  if ($mod4) {
    $data .= substr('====', $mod4);
  }
  return base64_decode($data);
}

// Function under progress
function pluralWord($sWord){
    $iLetterCount = strlen($sWord);

    if( strtolower($sWord[$iLetterCount-1]) == "y"){
        return substr($sWord, 0, $iLetterCount -1 )."ies";
    }

    return $sWord."s";
}

//! define function to set the data of hacker if any one want to Hack the system
//! And inform to the administrator with hacker details.
function setHackerDetails($sIP){

	//! Yet not completed.
}

//! brief function to get current age for database date formate
function fGetCurrentAge($dBirthdate) {
   return floor((strtotime(date('d-m-Y')) - strtotime($dBirthdate))/(60*60*24*365.2421896));
}

//! brief function to get date in Mysql Database format
function fGetDateInDBFormat($dDate){

	return date("Y-m-d", strtotime($dDate));
}

//! brief function to get time in Mysql time format
function fGetTimeInDBFormat($tTime){

	return date("h:i:s", strtotime($tTime));
}

//! brief function to get time in Mysql time format
function fGetTimeInPatArrivalFormat($tTime){

	return date("g:i A", strtotime($tTime));
}

//! brief function to increament latter
function fIncrementLetter($sLetter){

	$sAlphaChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	$sNextLetter = '';

	$indexOfLetter = strpos($sAlphaChars, $sLetter);
	
	if($indexOfLetter != 25){

		if($indexOfLetter+1 < strlen($sAlphaChars)){

			$sNextLetter = $sAlphaChars[$indexOfLetter+1];
		}
	}

	return $sNextLetter;
}

//! brief function to generate random passoword
function fGenerateRandomPassword($length = 10) {
    
    $alphabets = range('A','Z');
    $numbers = range('0','9');
    $final_array = array_merge($alphabets,$numbers);
         
    $password = '';
  
    while($length--) {
      $key = array_rand($final_array);
      $password .= $final_array[$key];
    }
  	
    return $password;
}

function fGetEncryptedPasswordHash($plainPassword){
    $salt = md5(uniqid('', true));
    $sSHAHash = hash("sha256", $salt.$plainPassword);
    $sEncryptedPassword = $salt.$sSHAHash;
    return $sEncryptedPassword;
}


//! Return Week number of date for that month
function fGetNumberOfWeekForDate($sDate) {
	$iDateTimestamp = date(strtotime($sDate));
	return date('W', $iDateTimestamp) - date('W', strtotime(date('Y-m-01', $iDateTimestamp))) + 1;
}


 /**
   * Return the total number of weeks of a given month.
   * @param int $year
   * @param int $month
   * @param int $start_day_of_week (0=Sunday ... 6=Saturday)
   * @return int
   */
function fGetTotalWeeksInMonth($iYear, $iMonth, $iStartDayOfWeek = 0)
{
	// Total number of days in the given month.
	$iNumOfDays = date("t", mktime(0,0,0,$iMonth,1,$iYear));

	// Count the number of times it hits $start_day_of_week.
	$iNumOfWeeks = 0;

	//if first day of month start after start day of week
	if(date('w', strtotime($iYear.'-'.$iMonth.'-1')) > $iStartDayOfWeek) {
		$iNumOfWeeks++;
	}

	for($i=1; $i<=$iNumOfDays; $i++)
	{
	  $iDayOfWeek = date('w', mktime(0,0,0,$iMonth,$i,$iYear));
	  
	  if($iDayOfWeek==$iStartDayOfWeek)
	    $iNumOfWeeks++;
	}

	return $iNumOfWeeks;
}

function hex2rgb($hex) {
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgb = array($r, $g, $b);
   //return implode(",", $rgb); // returns the rgb values separated by commas
   return $rgb; // returns an array with the rgb values
}

function convertNumber($number)
{
    list($integer, $fraction) = explode(".", (string) $number);

    $output = "";

    if ($integer{0} == "-")
    {
        $output = "negative ";
        $integer    = ltrim($integer, "-");
    }
    else if ($integer{0} == "+")
    {
        $output = "positive ";
        $integer    = ltrim($integer, "+");
    }

    if ($integer{0} == "0")
    {
        $output .= "zero";
    }
    else
    {
        $integer = str_pad($integer, 36, "0", STR_PAD_LEFT);
        $group   = rtrim(chunk_split($integer, 3, " "), " ");
        $groups  = explode(" ", $group);

        $groups2 = array();
        foreach ($groups as $g)
        {
            $groups2[] = convertThreeDigit($g{0}, $g{1}, $g{2});
        }

        for ($z = 0; $z < count($groups2); $z++)
        {
            if ($groups2[$z] != "")
            {
                $output .= $groups2[$z] . convertGroup(11 - $z) . (
                        $z < 11
                        && !array_search('', array_slice($groups2, $z + 1, -1))
                        && $groups2[11] != ''
                        && $groups[11]{0} == '0'
                            ? " and "
                            : ", "
                    );
            }
        }

        $output = rtrim($output, ", ");
    }

    if ($fraction > 0)
    {
        $output .= " point";
        for ($i = 0; $i < strlen($fraction); $i++)
        {
            $output .= " " . convertDigit($fraction{$i});
        }
    }

    return $output;
}

function convertThreeDigit($digit1, $digit2, $digit3)
{
    $buffer = "";

    if ($digit1 == "0" && $digit2 == "0" && $digit3 == "0")
    {
        return "";
    }

    if ($digit1 != "0")
    {
        $buffer .= convertDigit($digit1) . " hundred";
        if ($digit2 != "0" || $digit3 != "0")
        {
            $buffer .= " and ";
        }
    }

    if ($digit2 != "0")
    {
        $buffer .= convertTwoDigit($digit2, $digit3);
    }
    else if ($digit3 != "0")
    {
        $buffer .= convertDigit($digit3);
    }

    return $buffer;
}
function convertDigit($digit)
{
    switch ($digit)
    {
        case "0":
            return "zero";
        case "1":
            return "one";
        case "2":
            return "two";
        case "3":
            return "three";
        case "4":
            return "four";
        case "5":
            return "five";
        case "6":
            return "six";
        case "7":
            return "seven";
        case "8":
            return "eight";
        case "9":
            return "nine";
    }
}

function convertGroup($index)
{
    switch ($index)
    {
        case 11:
            return " decillion";
        case 10:
            return " nonillion";
        case 9:
            return " octillion";
        case 8:
            return " septillion";
        case 7:
            return " sextillion";
        case 6:
            return " quintrillion";
        case 5:
            return " quadrillion";
        case 4:
            return " trillion";
        case 3:
            return " billion";
        case 2:
            return " million";
        case 1:
            return " thousand";
        case 0:
            return "";
    }
}

function convertTwoDigit($digit1, $digit2)
{
    if ($digit2 == "0")
    {
        switch ($digit1)
        {
            case "1":
                return "ten";
            case "2":
                return "twenty";
            case "3":
                return "thirty";
            case "4":
                return "forty";
            case "5":
                return "fifty";
            case "6":
                return "sixty";
            case "7":
                return "seventy";
            case "8":
                return "eighty";
            case "9":
                return "ninety";
        }
    } else if ($digit1 == "1")
    {
        switch ($digit2)
        {
            case "1":
                return "eleven";
            case "2":
                return "twelve";
            case "3":
                return "thirteen";
            case "4":
                return "fourteen";
            case "5":
                return "fifteen";
            case "6":
                return "sixteen";
            case "7":
                return "seventeen";
            case "8":
                return "eighteen";
            case "9":
                return "nineteen";
        }
    } else
    {
        $temp = convertDigit($digit2);
        switch ($digit1)
        {
            case "2":
                return "twenty-$temp";
            case "3":
                return "thirty-$temp";
            case "4":
                return "forty-$temp";
            case "5":
                return "fifty-$temp";
            case "6":
                return "sixty-$temp";
            case "7":
                return "seventy-$temp";
            case "8":
                return "eighty-$temp";
            case "9":
                return "ninety-$temp";
        }
    }
}

function cleanString($text) {
    $utf8 = array(
        '/[áàâãªä]/u'   =>   'a',
        '/[ÁÀÂÃÄ]/u'    =>   'A',
        '/[ÍÌÎÏ]/u'     =>   'I',
        '/[íìîï]/u'     =>   'i',
        '/[éèêë]/u'     =>   'e',
        '/[ÉÈÊË]/u'     =>   'E',
        '/[óòôõºö]/u'   =>   'o',
        '/[ÓÒÔÕÖ]/u'    =>   'O',
        '/[úùûü]/u'     =>   'u',
        '/[ÚÙÛÜ]/u'     =>   'U',
        '/ç/'           =>   'c',
        '/Ç/'           =>   'C',
        '/ñ/'           =>   'n',
        '/Ñ/'           =>   'N',
        '/–/'           =>   '-', // UTF-8 hyphen to "normal" hyphen
        '/[’‘‹›‚]/u'    =>   ' ', // Literally a single quote
        '/[“”«»„]/u'    =>   ' ', // Double quote
        '/ /'           =>   ' ', // nonbreaking space (equiv. to 0x160)
    );
    return preg_replace(array_keys($utf8), array_values($utf8), $text);
}


function parseBreadcrumb($aBreadcrumb) {
    if($aBreadcrumb == null || empty($aBreadcrumb)) {
        /*$aBreadcrumb = array(array(
                "title"=>"Dashboard",
                "link"=>"userDashboard.php",
                "isActive"=>"true"
            ));*/
    }

    foreach ($aBreadcrumb as $aBreadcrumbItem) {
        $sClass = "";
        if($aBreadcrumbItem['isActive']) {
            $sClass .= "active";
        }
        $sTitle = $aBreadcrumbItem['title'];
        $sLink = $aBreadcrumbItem['link'];
        if(!$aBreadcrumbItem['isActive']) {
            $sTitle = "<small>{$sTitle}</small>";
        }
        ?>
            <li class="<?php echo $sClass; ?>"><a href="<?php echo $sLink; ?>"><?php echo $sTitle; ?></a></li>
        <?php
    }
}

function convertTime($dec)
{
    // start by converting to seconds
    $seconds = ($dec * 3600);
    // we're given hours, so let's get those the easy way
    $hours = floor($dec);
    // since we've "calculated" hours, let's remove them from the seconds variable
    $seconds -= $hours * 3600;
    // calculate minutes left
    $minutes = floor($seconds / 60);
    // remove those from seconds as well
    $seconds -= $minutes * 60;
    $seconds = floor($minutes);
    // return the time formatted HH:MM:SS
    return lz($hours).":".lz($minutes).":".lz($seconds);
}

// lz = leading zero
function lz($num)
{
    return (strlen($num) < 2) ? "0{$num}" : $num;
}

function parseIPDBreadcrumb($aBreadcrumb) {
    if($aBreadcrumb == null || empty($aBreadcrumb)) {
        $aBreadcrumb = array(array(
                "title"=>"Dashboard",
                "link"=>"userDashboard.php",
                "isActive"=>"true"
            ));
    }

    foreach ($aBreadcrumb as $aBreadcrumbItem) {
        $sClass = "classDividerBreadcrumb";
        if($aBreadcrumbItem['isActive']) {
            $sClass .= "active";
        }
        $sTitle = $aBreadcrumbItem['title'];
        $sLink = $aBreadcrumbItem['link'];
        //echo $sClass;
        ?>
            <li><a class="<?php echo $sClass; ?>" href="<?php echo $sLink; ?>"><?php echo $sTitle; ?></a></li>
        <?php
    }
}

//! Function for fetching receiver list for message sending..
function fGetReceiverListingForMessage($iMessageID){
    $aReceiverList = array();
    $DBMan = new DBConnManager();
    $conn =  $DBMan->getConnInstance();
    
    $sTable = 'message_recipients';

    $sSQuery = "SELECT `receiver`,`receiver_name` FROM `{$sTable}` WHERE `message_id` = '{$iMessageID}' AND `status` IN (1,2) ";

    $sSQueryR = $conn->query($sSQuery);
    if($sSQueryR!==FALSE){
        while($aRow = $sSQueryR->fetch_assoc()){
            $aReceiverList[] = $aRow;
        }
    }

    return $aReceiverList;
}

//! function  to generate a token
function fGenerateToken($iCphUserId){
    $DBMan = new DBConnManager();
    $conn =  $DBMan->getConnInstance();
    $is_used = 0;

    $sToken = md5(uniqid(rand(), TRUE));

    $sTable = "direct_ehr_login_token";

    $sQuery = "INSERT INTO `{$sTable}` (`token`, `is_used`, `user_id`, `added_by`, `added_on`)
        VALUES ('{$sToken}',{$is_used},{$iCphUserId},{$iCphUserId}, NOW())";   
        // print_r($sQuery);die;
    $sIQueryR = $conn->query($sQuery);
    
    if($sIQueryR > 0){
        $iInsertID = $conn->insert_id;
    }
    return $sToken;
   
}

/*
    Function to check and replace empty parameter with a null 
    Added by Akshay Sutar on 27-06-2018
*/
    function checkParameterValue($sParameter){
        if($sParameter == ""){
            // null is treated as empty string, so return null as string
            return "null";
        }else{
            return "'".$sParameter."'";
        }
    }

/*
    Function to generate combinations 
    Created by Akshay Sutar on 10-01-2019
    Supports till combination on 3 
*/ 
    function generateCombinations($aInput){
        $aCombintations = [];

        $iSize = count($aInput);
        $aTemp = [];

        switch($iSize){
            case 1:
                $aCombintations = $aInput;
            break;

            case 2:
                for($iii=0; $iii < $iSize; ++$iii){
                    for($jjj=0; $jjj < $iSize; ++$jjj){
                        $thisCombination = $aInput[$iii]."|".$aInput[$jjj];
                        if( !in_array($thisCombination, $aTemp) ){
                            if($iii == $jjj){
                                continue;
                            }
                            $aCombintations[] = $thisCombination;
                            $aTemp[] =  $thisCombination;
                        }
                    }
                }
            break;

            case 3:
                for($iii=0; $iii < $iSize; ++$iii){
                    for($jjj=0; $jjj < $iSize; ++$jjj){
                        for($kkk=0; $kkk < $iSize; ++$kkk){
                            if($iii == $jjj || $iii == $kkk || $jjj==$kkk){
                                continue;
                            }
                            $thisCombination = $aInput[$iii]."|".$aInput[$jjj]."|".$aInput[$kkk];
                            if( !in_array($thisCombination, $aTemp) ){
                                $aCombintations[] = $thisCombination;
                                $aTemp[] =  $thisCombination;
                            }
                        }                            
                    }
                }
            break;
        }

        return $aCombintations;
    }   

    /*
        Function to generate combinations for an array
        Created by Akshay Sutar on 17-01-2019
    */
    function generateCombinationForArray($aInput){
        $aCombintations = [];
        $iCount = count($aInput);
        $aArrayKeys = array_keys($aInput);
        switch ($iCount) {
            case 1:
                $iIndex1 = $aArrayKeys[0];
                $iCount1 = count($aInput[$iIndex1]);

                //$aCombintations[] = $aInput;
                for($iii=0; $iii< $iCount1; ++$iii){
                    $sVal1 = $aInput[$iIndex1][$iii];
                    $aCombintations[] = array($iIndex1=>$sVal1);
                }
                break;

            case 2:
                
                $iIndex1 = $aArrayKeys[0];
                $iIndex2 = $aArrayKeys[1];

                $iCount1 = count($aInput[$iIndex1]);
                $iCount2 = count($aInput[$iIndex2]);

                for($iii=0; $iii < $iCount1; ++$iii){
                    for($jjj=0; $jjj < $iCount2; ++$jjj){
                        $sVal1 = $aInput[$iIndex1][$iii];
                        $sVal2 = $aInput[$iIndex2][$jjj];
                        
                        if($sVal1 == $sVal2){
                            continue;
                        }
                        $aCombintations[] = array($iIndex1=>$sVal1,$iIndex2=>$sVal2);
                    }
                }
                break;

            case 3:
                $iIndex1 = $aArrayKeys[0];
                $iIndex2 = $aArrayKeys[1];
                $iIndex3 = $aArrayKeys[2];

                $iCount1 = count($aInput[$iIndex1]);
                $iCount2 = count($aInput[$iIndex2]);
                $iCount3 = count($aInput[$iIndex3]);

                for($iii=0; $iii < $iCount1; ++$iii){
                    for($jjj=0; $jjj < $iCount2; ++$jjj){
                        for($kkk=0; $kkk < $iCount2; ++$kkk){
                            $sVal1 = $aInput[$iIndex1][$iii];
                            $sVal2 = $aInput[$iIndex2][$jjj];
                            $sVal3 = $aInput[$iIndex3][$kkk];
                            
                            if($sVal1 == $sVal2 || $sVal1 == $sVal3 || $sVal2 == $sVal3 ){
                                continue;
                            }
                            $aCombintations[] = array($iIndex1=>$sVal1,$iIndex2=>$sVal2,$iIndex3=>$sVal3);
                        }                                
                    }
                }
                break;
        }
        return $aCombintations;
    }

    // Call this at each point of interest, passing a descriptive string for profiling..
    function funProfilingFlag($sStringToPrint){
        global $__tProfilingTime, $__sProfilingName;

        $__tProfilingTime[] = microtime(true);
        $__sProfilingName[] = $sStringToPrint;
    }

    // Call this funtion when you're done and want to see the results of profiling..
    function funProfilingResult(){
        global $__tProfilingTime, $__sProfilingName;

        $iSize = count($__tProfilingTime);
        $sExecutionDuration = "";
        if($iSize > 0){
            for($iii=0; $iii<$iSize - 1; $iii++){
                $sExecutionDuration .= "<br><b>{$__sProfilingName[$iii]}</b><br>";
                $sExecutionDuration .= sprintf("&nbsp;&nbsp;&nbsp;%f<br>", $__tProfilingTime[$iii+1]-$__tProfilingTime[$iii]);
            }

            $sExecutionDuration .= "<b>{$__sProfilingName[$iSize-1]}</b><br>";            
        }

        return $sExecutionDuration;
    }

    //! function to purify html content..
    function purifyHTML($sDirtyHTML){
        return (new HTMLPurifier(HTMLPurifier_Config::createDefault()))->purify($sDirtyHTML);
    }

    //! function to verify access token to prevent from csrf attach..
    function validateCSRFAccessToken($sStoredAccessToken,$sPostAccessToken){
        if(CSRF_ATTACK_PREVENTION){            
            //! Invalid data to compare..
            if(is_null($sPostAccessToken) || $sPostAccessToken == '' || is_null($sStoredAccessToken) || $sStoredAccessToken == ''){
                return false;
            }
            //! Compare session token with post token
            if(!hash_equals($sStoredAccessToken, $sPostAccessToken)){
                return false;
            }            
        }
        return true;
    }

    //! Function to generate random string..
    function generateRandomString($iLength = 20) {
        $sCharacter = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        
        $sCharacterLength = strlen($sCharacter);
        $sRandomString = '';

        for ($iii = 0; $iii < $iLength; $iii++) {
            $sRandomString .= $sCharacter[rand(0, $sCharacterLength - 1)];
        }

        return $sRandomString;
    }

    //! Function for fetching weather information..
    function fGetChildFeedingInformationByWeatherTemp($iTemperature){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $aPrecautions = array('title' => '', 'precautions' => array());
        
        $sTable = 'child_food_details_by_food';

        $sSQuery = "SELECT `{$sTable}`.`info`,`{$sTable}`.`title` FROM `{$sTable}` WHERE `min_temp` >= '{$iTemperature}' AND `max_temp` <= '{$iTemperature}' AND `deleted` = 0";

        $sSQueryR = $conn->query($sSQuery);
        if($sSQueryR!==FALSE){
            while($aRow = $sSQueryR->fetch_assoc()){
                $aPrecautions['title'] = $aRow['title'];
                $aPrecautions['precautions'][] = $aRow['info'];
            }
        }

        return $aPrecautions;
    }

    //! Function for fetching weather information..
    function cityList(){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        
        $sTable = 'city_master';
        $aCityMaster = array();

        $sSQuery = "SELECT `city` FROM `{$sTable}`";

        $sSQueryR = $conn->query($sSQuery);
        if($sSQueryR!==FALSE){
            while($aRow = $sSQueryR->fetch_assoc()){
                $aCityMaster[] = $aRow;
            }
        }

        return $aCityMaster;
    }

    //! Function for fetching weather information..
    function getStateCountryByCity($sCity){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        
        $sTable = 'city_master';
        $aStateCountryList = array();

        $sSQuery = "SELECT `state`, `country` FROM `{$sTable}` WHERE `city` = '{$sCity}'";

        $sSQueryR = $conn->query($sSQuery);
        if($sSQueryR!==FALSE){
            $aRow = $sSQueryR->fetch_assoc();
            $aStateCountryList = $aRow;
        }

        return $aStateCountryList;
    }

    //! Function for fetching weather information..
    function fGetChildDetailsByParentID($iParentID){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        
        $sTable = 'parent_child_details';
        $aChildDetails = array();

        $sSQuery = "SELECT * FROM `{$sTable}` WHERE `parent_id` = '{$iParentID}' AND `deleted` = 0";

        $sSQueryR = $conn->query($sSQuery);
        if($sSQueryR!==FALSE){
            while($aRow = $sSQueryR->fetch_assoc()){
                $aChildDetails[] = $aRow;
            }
        }

        return $aChildDetails;
    }

    //! Function for fetching weather information..
    function fGetParentIDByUserID($iUserID){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        
        $sTable = 'parent_details';
        $iParentID = 0;

        $sSQuery = "SELECT `parent_id` FROM `{$sTable}` WHERE `user_id` = '{$iUserID}' AND `deleted` = 0";

        $sSQueryR = $conn->query($sSQuery);
        if($sSQueryR!==FALSE){
            $aRow = $sSQueryR->fetch_assoc();
            $iParentID = $aRow['parent_id'];
        }

        return $iParentID;
    }

    function fGetChildUniqueNumber(){
        return "VA-".strtoupper(generateRandomString(6));
    }

    //! function  to generate a token
    function fAddChildDetails($iParentID, $sChildNumber, $sChildName, $dChildDOB){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $dAddedOn = date('Y-m-d H:i:s');

        $sTable = "parent_child_details";

        $sQuery = "INSERT INTO `{$sTable}` (`child_id`, `child_no`, `parent_id`, `child_name`, `date_of_birth`, `added_on`, `deleted`)
            VALUES (NULL, '{$sChildNumber}', '{$iParentID}', '{$sChildName}', '{$dChildDOB}', '{$dAddedOn}', '0')";   
        $sIQueryR = $conn->query($sQuery);
        
        if($sIQueryR > 0){
            $iInsertID = $conn->insert_id;
        }

        return $iInsertID;
       
    }

    //! function  to generate a token
    function fGetVaccinationGroupDetails(){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        
        $sTable = 'vaccination_group_details';
        $aGroupDetails = array();

        $sSQuery = "SELECT * FROM `{$sTable}` WHERE `deleted` = 0";

        $sSQueryR = $conn->query($sSQuery);
        if($sSQueryR!==FALSE){
            while($aRow = $sSQueryR->fetch_assoc()){
                $aGroupDetails[] = $aRow;
            }
        }

        return $aGroupDetails;
       
    }

    //! function  to generate a token
    function fGetVaccinationListByAge($iMinAge,$iMaxAge,$sChildNo){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        
        $sVaccineMaster = 'vaccination_master';
        $sIdealDateTbl = 'vaccination_ideal_date_details';
        $sReceivedDateTbl = 'vaccination_completed_status';
        $sChildPArentTbl = 'parent_child_details';
        $aVaccinationMaster = array();

        $sSQuery = "SELECT A.*, B.`ideal_date`, C.`received_date`, C.`remark` FROM `{$sVaccineMaster}` AS A LEFT JOIN `{$sIdealDateTbl}` AS B ON B.`vaccination_id` = A.`vaccination_id` LEFT JOIN `{$sReceivedDateTbl}` AS C ON C.`vaccination_id` = A.`vaccination_id` AND C.`child_id` = B.`child_id` AND C.`deleted`= 0  WHERE `no_of_days` >= '{$iMinAge}' AND `no_of_days` <= '{$iMaxAge}' AND A.`deleted` = 0 AND B.`deleted` = 0 AND B.`child_id` IN (SELECT `child_id` FROM `{$sChildPArentTbl}` WHERE `child_no` = '{$sChildNo}')";
        
        $sSQueryR = $conn->query($sSQuery);
        if($sSQueryR!==FALSE){
            while($aRow = $sSQueryR->fetch_assoc()){
                $aRow['ideal_date'] = (isset($aRow['ideal_date']) && $aRow['ideal_date'] != null) ? date('d-m-Y', strtotime($aRow['ideal_date'])) : '';
                $aRow['received_date'] = (isset($aRow['received_date']) && $aRow['received_date'] != null) ? date('d-m-Y', strtotime($aRow['received_date'])) : '';
                $aRow['remark'] = (isset($aRow['remark']) && $aRow['remark'] != null) ? $aRow['remark'] : '';
                $aVaccinationMaster[] = $aRow;
            }
        }

        return $aVaccinationMaster;
       
    }

    //! function  to generate a token
    function fGetVaccinationMaster(){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        
        $sTable = 'vaccination_master';
        $aVaccinationMaster = array();

        $sSQuery = "SELECT * FROM `{$sTable}` WHERE `deleted` = 0";

        $sSQueryR = $conn->query($sSQuery);
        if($sSQueryR!==FALSE){
            while($aRow = $sSQueryR->fetch_assoc()){
                $aVaccinationMaster[] = $aRow;
            }
        }

        return $aVaccinationMaster;
       
    }

    //! function  to generate a token
    function fAddIdealDateDetails($iVaccinationID, $iChildID, $iParentID, $dIdealDate){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $dAddedOn = date('Y-m-d H:i:s');

        $sTable = "vaccination_ideal_date_details";

        $sQuery = "INSERT INTO `{$sTable}` (`id`, `vaccination_id`, `child_id`, `parent_id`, `ideal_date`, `added_on`, `deleted`)
            VALUES (NULL, '{$iVaccinationID}', '{$iChildID}', '{$iParentID}', '{$dIdealDate}', '{$dAddedOn}', '0')";   
        $sIQueryR = $conn->query($sQuery);
        
        if($sIQueryR > 0){
            $iInsertID = $conn->insert_id;
        }

        return $iInsertID;
       
    }

    //! Function for fetching weather information..
    function fGetChildIDByChildNo($sChildNo){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        
        $sTable = 'parent_child_details';
        $iChildID = 0;

        $sSQuery = "SELECT `child_id` FROM `{$sTable}` WHERE `child_no` = '{$sChildNo}' AND `deleted` = 0";

        $sSQueryR = $conn->query($sSQuery);
        if($sSQueryR!==FALSE){
            $aRow = $sSQueryR->fetch_assoc();
            $iChildID = $aRow['child_id'];
        }

        return $iChildID;
    }

    //! function  to generate a token
    function fAddReceivedDateDetails($iVaccinationID, $iChildID, $iParentID, $dReceivedDate, $sRemark){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $dAddedOn = date('Y-m-d H:i:s');

        $sTable = "vaccination_completed_status";
        $bResult = false;

        $sQuery = "INSERT INTO `{$sTable}` (`id`, `vaccination_id`, `child_id`, `parent_id`, `received_date`, `remark`, `added_on`, `deleted`)
            VALUES (NULL, '{$iVaccinationID}', '{$iChildID}', '{$iParentID}', '{$dReceivedDate}', '{$sRemark}', '{$dAddedOn}', '0')";   
        $sIQueryR = $conn->query($sQuery);
        
        if($sIQueryR > 0){
            $bResult = true;
        }

        return $bResult;
       
    }

    //! function  to generate a token
    function fAddNewVaccine($sVaccineName, $iNoOfDays){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = "vaccination_master";
        $iInsertID = 0;

        $sQuery = "INSERT INTO `{$sTable}` (`vaccination_id`, `vaccination_name`, `no_of_days`, `deleted`)
            VALUES (NULL, '{$sVaccineName}', '{$iNoOfDays}', '0')";   
        
        $sIQueryR = $conn->query($sQuery);
        
        if($sIQueryR > 0){
            $iInsertID = $conn->insert_id;
        }

        return $iInsertID;
       
    }

    //! function  to generate a token
    function fGetAllChildDetails(){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        
        $sTable = 'parent_child_details';
        $aAllChilds = array();

        $sSQuery = "SELECT * FROM `{$sTable}` WHERE `deleted` = 0";

        $sSQueryR = $conn->query($sSQuery);
        if($sSQueryR!==FALSE){
            while($aRow = $sSQueryR->fetch_assoc()){
                $aRow['date_of_birth'] = (isset($aRow['date_of_birth']) && $aRow['date_of_birth'] != null) ? date('d-m-Y', strtotime($aRow['date_of_birth'])) : '';
                $aAllChilds[] = $aRow;
            }
        }

        return $aAllChilds;
       
    }
?>