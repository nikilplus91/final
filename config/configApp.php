<?php

//! @file Single config file which includes all config files
include_once dirname(__FILE__)."/../lib/autoload.php";
require_once dirname(__FILE__)."/config.app.php";
require_once dirname(__FILE__)."/config.database.php";
require_once dirname(__FILE__)."/config.Email_SMS.php";

date_default_timezone_set(DATE_DEFAULT_TIMEZONE);

require_once ABS_PATH_TO_APP.'vendor/autoload.php';
?>