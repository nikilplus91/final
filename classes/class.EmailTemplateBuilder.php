<?php

//! @brief - Class EmailTemplate
//! It is used to create sigle and two column email template.
//! It also include the emial header and footer functionality.

class EmailTemplateBuilder{

	public $sEmailTemplate;
	private $sHeader;
	private $sFooter;
	private $sLogoURL;
	private $sORGName;

	function __construct(){

		$this->sLogoURL = ORG_LOGO_URL;
		$this->sORGName = CONFIG_PROJECT_NAME;

		$this->sEmailTemplate = $this->fHeader();
	}

	public function fClose(){

		$this->sEmailTemplate .= $this->fFooter();
	}

	private function fHeader(){

		$sHeader = <<<EOB
		<div style="padding: 10px;background-color:#f2f2f2;color:#333;font-family:'Helvetica Neue';">
			<div style='border-radius:10px 0px 0px 0px;background-color:#FFF;border:1px solid #FFF;'>
				<div style='padding: 20px 50px 0px 50px;'>
					<img src='{$this->sLogoURL}' style='float:right;max-height:50px !important;' />
				</div>
			</div>
EOB;

		return $sHeader;
	}

	private function fFooter(){

		$sFooter = <<<EOB
			<div style='text-align:center;font-size:14px;padding-top:15px;'>
				<span style='padding-left:0px;color:#97a3b1;'>Tike On Time Powered By Plus91 Technologies Private Limited</span>
			</div>
		</div>
EOB;


		return $sFooter;
	}

	public function fCreateOneColumnTemplate($sContent){

		$this->sEmailTemplate .= <<<EOB
		<div style='border-radius:10px;background-color:#FFF;border:1px solid #FFF;margin: -60px 0px 0px 0px;font-size:14px;'>
			<div style='padding: 20px 50px 50px 50px;'>
				{$sContent}	
			</div>
		</div>
EOB;

		return $sEmailTemplate;
	}
}
?>