<?php
include_once "config/configApp.php";
include_once ABS_PATH_TO_APP."functions.php";
include_once ABS_PATH_TO_APP."classes/class.SessionManager.php";
include_once ABS_PATH_TO_APP."classes/class.User.php";

$oSessionManager = new SessionManager();

$sUsername = addslashes($_POST['username']);
$sPassword = addslashes($_POST['password']);

$bAppRequest = isset($_POST['appRequest']) ? ($_POST['appRequest'] == 1) : false;

//! Redirect to dashboard if login successful
if($oSessionManager->fAuthenticateUser($sUsername, $sPassword)){
    $_SESSION['sUserName'] = $oSessionManager->sUsername;
    $iUserID = $oSessionManager->iUserID;
    
    //! Fetching parent child details..
    $aChildDetails = fGetChildDetailsByParentID(fGetParentIDByUserID($iUserID));

    if(!$bAppRequest){
        if(!empty($aChildDetails)){
            header('Location: vaccinationList.php?iSuccess=1');
        }else{
            header('Location: childSignUp.php?iSuccess=1');
        }
    }else{
        if(!empty($aChildDetails)){
            $iResult = 1;
            header("Content-Type: application/json");
            echo(json_encode($iResult));
        }else{
            $iResult = 2;
            header("Content-Type: application/json");
            echo(json_encode($iResult));
        }
    }
}else{
    if(!$bAppRequest){
        header('Location: login.php?iSuccess=0');
    }else{
        $iResult = 3;
        header("Content-Type: application/json");
        echo(json_encode($iResult));
    }
}
?>