<?php
//! It contains the Email template constant variable
include_once "config/configApp.php";
require_once ABS_PATH_TO_APP."config/config.SMSTemplate.php";
require_once ABS_PATH_TO_APP."config/config.Email_SMS.php";
require_once ABS_PATH_TO_APP."classes/class.SMS.php";

//! brief function to send SMS using SMS class
//! Parameter : Recipient details, SMS template flag, SMS info
//! $aRecipientNo - Array - It contains all the recipient numbers
//! $aRecipientName - Array - It contains all the recipient names
//! $iSMSTemplateFlag - Integer - It contains SMS template flag value
//! $aSMSInfo - Array - It contains the SMS info which is used toe create SMS template with dynamic values.
//! $iSenderNo - Array - It contains sender no and it is not mandatory
//! $sSenderName - Array - It contains sender name and it is not mandatory
//! Return : TRUE -> Send SMS successfully, FALSE -> Not send and gives the error message
function fSendSMS($aRecipientNo,$aRecipientName,$iSMSTemplateFlag,$aSMSInfo,$iSenderNo,$sSenderName){

    $iResult = FALSE;

    if(SMS_ON == FALSE){
        return TRUE;
    }

    if(TEST_SMS == TRUE){
        $aRecipientNo = array(TEST_SMS_NO);
    }

    //! Create SMS template
    $sSMSTemplate = fCreateSMSTemplate($iSMSTemplateFlag,$aSMSInfo);

    if($sSMSTemplate != '0'){

        $iRecipientCount = count($aRecipientNo);

        $oSMS = new SMS($aRecipientNo[0],$aRecipientName[0],$sSMSTemplate,$iSenderNo,$sSenderName);

        //! Check when SMS created successfully
        if($oSMS->iInsertSmsId > 0){

            if($iRecipientCount > 1){

                //! Added Recipient if it is more that one
                for($iii=1;$iii<$iRecipientCount;$iii++){
                    $oSMS->addRecipient($aRecipientNo[$iii],$aRecipientName[$iii]);
                }
            }
        }

        //! Send SMS
        $iResult = $oSMS->sendSMSNow();
    }

    return $iResult;
}

//! brief function to create a SMS template
//! Parameter - SMS template flag, SMS dynamic info
//! Return - string - SMS Template for successful result, 0 for unsuccessful
function fCreateSMSTemplate($iSMSTemplateFlag,$aSMSInfo){

    $sSMSTemplate = ''; 

    if($iSMSTemplateFlag == SEND_SMS_FOR_BCG_VACCINE){

        $sVaccineName = $aSMSInfo['sVaccineName'];
        $dVaccineDate = $aSMSInfo['dVaccineDate'];
        $sParentName =  $aSMSInfo['sParentName'];
        $sChildNumber=  $aSMSInfo['sChildNumber'];

        $sSMSTemplate="Dear {$sParentName}, Your child {$sChildNumber}, is due for Vaccination {$sVaccineName} on {$dVaccineDate}. Kindly visit your nearby centre. It's Good To Know it Early When it Comes To Health! - ".CONFIG_PROJECT_NAME;

    }else if($iSMSTemplateFlag == SEND_SMS_FOR_HEPATITIS_B_BIRTH_DOSE_VACCINE){

        $sVaccineName = $aSMSInfo['sVaccineName'];
        $dVaccineDate = $aSMSInfo['dVaccineDate'];
        $sParentName =  $aSMSInfo['sParentName'];
        $sChildNumber=  $aSMSInfo['sChildNumber'];

        $sSMSTemplate="Dear {$sParentName}, Your child {$sChildNumber}, is due for Vaccination  {$sVaccineName} on {$dVaccineDate}.Kindly visit your nearby centre. It's Good To Know it Early When it Comes To Health! - ".CONFIG_PROJECT_NAME;

    }else if($iSMSTemplateFlag == SEND_SMS_FOR_OPV_BIRTH_DOSE_VACCINE){

        $sVaccineName = $aSMSInfo['sVaccineName'];
        $dVaccineDate = $aSMSInfo['dVaccineDate'];
        $sParentName = $aSMSInfo['sParentName'];
        $sChildNumber=  $aSMSInfo['sChildNumber'];

        $sSMSTemplate="Dear {$sParentName}, Your child {$sChildNumber}, is due for Vaccination  {$sVaccineName} on {$dVaccineDate}.Kindly visit your nearby centre. It's Good To Know it Early When it Comes To Health! - ".CONFIG_PROJECT_NAME;

    }else if($iSMSTemplateFlag == SEND_SMS_FOR_OPV_1_VACCINE){

        $sVaccineName = $aSMSInfo['sVaccineName'];
        $dVaccineDate = $aSMSInfo['dVaccineDate'];
        $sParentName = $aSMSInfo['sParentName'];
        $sChildNumber=  $aSMSInfo['sChildNumber'];

        $sSMSTemplate="Dear {$sParentName}, Your child {$sChildNumber}, is due for Vaccination  {$sVaccineName} on {$dVaccineDate}.Kindly visit your nearby centre. It's Good To Know it Early When it Comes To Health! - ".CONFIG_PROJECT_NAME;

    }else if($iSMSTemplateFlag == SEND_SMS_FOR_OPV_2_VACCINE){

        $sVaccineName = $aSMSInfo['sVaccineName'];
        $dVaccineDate = $aSMSInfo['dVaccineDate'];
        $sParentName = $aSMSInfo['sParentName'];
        $sChildNumber=  $aSMSInfo['sChildNumber'];

        $sSMSTemplate="Dear {$sParentName}, Your child {$sChildNumber}, is due for Vaccination  {$sVaccineName} on {$dVaccineDate}.Kindly visit your nearby centre. It's Good To Know it Early When it Comes To Health! - ".CONFIG_PROJECT_NAME;


    }else if($iSMSTemplateFlag == SEND_SMS_FOR_OPV_3_VACCINE){

        $sVaccineName = $aSMSInfo['sVaccineName'];
        $dVaccineDate = $aSMSInfo['dVaccineDate'];
        $sParentName = $aSMSInfo['sParentName'];
        $sChildNumber=  $aSMSInfo['sChildNumber'];

        $sSMSTemplate="Dear {$sParentName}, Your child {$sChildNumber}, is due for Vaccination  {$sVaccineName} on {$dVaccineDate}.Kindly visit your nearby centre. It's Good To Know it Early When it Comes To Health! - ".CONFIG_PROJECT_NAME;


    }else if($iSMSTemplateFlag == SEND_SMS_FOR_IPV_VACCINE){

        $sVaccineName = $aSMSInfo['sVaccineName'];
        $dVaccineDate = $aSMSInfo['dVaccineDate'];
        $sParentName = $aSMSInfo['sParentName'];
        $sChildNumber=  $aSMSInfo['sChildNumber'];

        $sSMSTemplate="Dear {$sParentName}, Your child {$sChildNumber}, is due for Vaccination  {$sVaccineName} on {$dVaccineDate}.Kindly visit your nearby centre. It's Good To Know it Early When it Comes To Health! - ".CONFIG_PROJECT_NAME;

    }else if($iSMSTemplateFlag == SEND_SMS_FOR_PENTAVELANT_1_VACCINE){

        $sVaccineName = $aSMSInfo['sVaccineName'];
        $dVaccineDate = $aSMSInfo['dVaccineDate'];
        $sParentName = $aSMSInfo['sParentName'];
        $sChildNumber=  $aSMSInfo['sChildNumber'];

        $sSMSTemplate="Dear {$sParentName}, Your child {$sChildNumber}, is due for Vaccination  {$sVaccineName} on {$dVaccineDate}.Kindly visit your nearby centre. It's Good To Know it Early When it Comes To Health! - ".CONFIG_PROJECT_NAME;



    }else if($iSMSTemplateFlag == SEND_SMS_FOR_PENTAVELANT_2_VACCINE){

        $sVaccineName = $aSMSInfo['sVaccineName'];
        $dVaccineDate = $aSMSInfo['dVaccineDate'];
        $sParentName = $aSMSInfo['sParentName'];
        $sChildNumber=  $aSMSInfo['sChildNumber'];


        $sSMSTemplate="Dear {$sParentName}, Your child {$sChildNumber}, is due for Vaccination  {$sVaccineName} on {$dVaccineDate}.Kindly visit your nearby centre. It's Good To Know it Early When it Comes To Health! - ".CONFIG_PROJECT_NAME;

    }else if($iSMSTemplateFlag == SEND_SMS_FOR_PENTAVELANT_3_VACCINE){

        $sVaccineName = $aSMSInfo['sVaccineName'];
        $dVaccineDate = $aSMSInfo['dVaccineDate'];
        $sParentName = $aSMSInfo['sParentName'];
        $sChildNumber=  $aSMSInfo['sChildNumber'];


        $sSMSTemplate="Dear {$sParentName}, Your child {$sChildNumber}, is due for Vaccination  {$sVaccineName} on {$dVaccineDate}.Kindly visit your nearby centre. It's Good To Know it Early When it Comes To Health! - ".CONFIG_PROJECT_NAME;

    }else if($iSMSTemplateFlag == SEND_SMS_FOR_ROTAVIRUS_1_VACCINE){

        $sVaccineName = $aSMSInfo['sVaccineName'];
        $dVaccineDate = $aSMSInfo['dVaccineDate'];
        $sParentName = $aSMSInfo['sParentName'];
        $sChildNumber=  $aSMSInfo['sChildNumber'];


        $sSMSTemplate="Dear {$sParentName}, Your child {$sChildNumber}, is due for Vaccination  {$sVaccineName} on {$dVaccineDate}.Kindly visit your nearby centre. It's Good To Know it Early When it Comes To Health! - ".CONFIG_PROJECT_NAME;

    }else if($iSMSTemplateFlag == SEND_SMS_FOR_ROTAVIRUS_2_VACCINE){

        $sVaccineName = $aSMSInfo['sVaccineName'];
        $dVaccineDate = $aSMSInfo['dVaccineDate'];
        $sParentName = $aSMSInfo['sParentName'];
        $sChildNumber=  $aSMSInfo['sChildNumber'];


        $sSMSTemplate="Dear {$sParentName}, Your child {$sChildNumber}, is due for Vaccination  {$sVaccineName} on {$dVaccineDate}.Kindly visit your nearby centre. It's Good To Know it Early When it Comes To Health! - ".CONFIG_PROJECT_NAME;

    }else if($iSMSTemplateFlag == SEND_SMS_FOR_ROTAVIRUS_3_VACCINE){

        $sVaccineName = $aSMSInfo['sVaccineName'];
        $dVaccineDate = $aSMSInfo['dVaccineDate'];
        $sParentName = $aSMSInfo['sParentName'];
        $sChildNumber=  $aSMSInfo['sChildNumber'];


        $sSMSTemplate="Dear {$sParentName}, Your child {$sChildNumber}, is due for Vaccination  {$sVaccineName} on {$dVaccineDate}.Kindly visit your nearby centre. It's Good To Know it Early When it Comes To Health! - ".CONFIG_PROJECT_NAME;

    }else if($iSMSTemplateFlag == SEND_SMS_FOR_MEASLES_1_VACCINE){

        $sVaccineName = $aSMSInfo['sVaccineName'];
        $dVaccineDate = $aSMSInfo['dVaccineDate'];
        $sParentName = $aSMSInfo['sParentName'];
        $sChildNumber=  $aSMSInfo['sChildNumber'];


        $sSMSTemplate="Dear {$sParentName}, Your child {$sChildNumber}, is due for Vaccination  {$sVaccineName} on {$dVaccineDate}.Kindly visit your nearby centre. It's Good To Know it Early When it Comes To Health! - ".CONFIG_PROJECT_NAME;

    }else if($iSMSTemplateFlag == SEND_SMS_FOR_VITAMIN_A_1_VACCINE){

        $sVaccineName = $aSMSInfo['sVaccineName'];
        $dVaccineDate = $aSMSInfo['dVaccineDate'];
        $sParentName = $aSMSInfo['sParentName'];
        $sChildNumber=  $aSMSInfo['sChildNumber'];


        $sSMSTemplate="Dear {$sParentName}, Your child {$sChildNumber}, is due for Vaccination  {$sVaccineName} on {$dVaccineDate}.Kindly visit your nearby centre. It's Good To Know it Early When it Comes To Health! - ".CONFIG_PROJECT_NAME;

    }else if($iSMSTemplateFlag == SEND_SMS_FOR_DPT_1_VACCINE){

        $sVaccineName = $aSMSInfo['sVaccineName'];
        $dVaccineDate = $aSMSInfo['dVaccineDate'];
        $sParentName = $aSMSInfo['sParentName'];
        $sChildNumber=  $aSMSInfo['sChildNumber'];

        $sSMSTemplate="Dear {$sParentName}, Your child {$sChildNumber}, is due for Vaccination  {$sVaccineName} on {$dVaccineDate}.Kindly visit your nearby centre. It's Good To Know it Early When it Comes To Health! - ".CONFIG_PROJECT_NAME;

    }else if($iSMSTemplateFlag == SEND_SMS_FOR_OPV_4_VACCINE){

        $sVaccineName = $aSMSInfo['sVaccineName'];
        $dVaccineDate = $aSMSInfo['dVaccineDate'];
        $sParentName = $aSMSInfo['sParentName'];
        $sChildNumber=  $aSMSInfo['sChildNumber'];

        $sSMSTemplate="Dear {$sParentName}, Your child {$sChildNumber}, is due for Vaccination  {$sVaccineName} on {$dVaccineDate}.Kindly visit your nearby centre. It's Good To Know it Early When it Comes To Health! - ".CONFIG_PROJECT_NAME;

    }else if($iSMSTemplateFlag == SEND_SMS_FOR_MEASLES_2_VACCINE){

        $sVaccineName = $aSMSInfo['sVaccineName'];
        $dVaccineDate = $aSMSInfo['dVaccineDate'];
        $sParentName = $aSMSInfo['sParentName'];
        $sChildNumber=  $aSMSInfo['sChildNumber'];

        $sSMSTemplate="Dear {$sParentName}, Your child {$sChildNumber}, is due for Vaccination  {$sVaccineName} on {$dVaccineDate}.Kindly visit your nearby centre. It's Good To Know it Early When it Comes To Health! - ".CONFIG_PROJECT_NAME;

    }else if($iSMSTemplateFlag == SEND_SMS_FOR_VITAMIN_A_1_VACCINE){

        $sVaccineName = $aSMSInfo['sVaccineName'];
        $dVaccineDate = $aSMSInfo['dVaccineDate'];
        $sParentName = $aSMSInfo['sParentName'];
        $sChildNumber=  $aSMSInfo['sChildNumber'];

        $sSMSTemplate="Dear {$sParentName}, Your child {$sChildNumber}, is due for Vaccination  {$sVaccineName} on {$dVaccineDate}.Kindly visit your nearby centre. It's Good To Know it Early When it Comes To Health! - ".CONFIG_PROJECT_NAME;

    }else if($iSMSTemplateFlag == SEND_SMS_FOR_VITAMIN_A_2_VACCINE){

        $sVaccineName = $aSMSInfo['sVaccineName'];
        $dVaccineDate = $aSMSInfo['dVaccineDate'];
        $sParentName = $aSMSInfo['sParentName'];
        $sChildNumber=  $aSMSInfo['sChildNumber'];

        $sSMSTemplate="Dear {$sParentName}, Your child {$sChildNumber}, is due for Vaccination  {$sVaccineName} on {$dVaccineDate}.Kindly visit your nearby centre. It's Good To Know it Early When it Comes To Health! - ".CONFIG_PROJECT_NAME;

    }else if($iSMSTemplateFlag == SEND_SMS_FOR_VITAMIN_A_3_VACCINE){

        $sVaccineName = $aSMSInfo['sVaccineName'];
        $dVaccineDate = $aSMSInfo['dVaccineDate'];
        $sParentName = $aSMSInfo['sParentName'];
        $sChildNumber=  $aSMSInfo['sChildNumber'];

        $sSMSTemplate="Dear {$sParentName}, Your child {$sChildNumber}, is due for Vaccination  {$sVaccineName} on {$dVaccineDate}.Kindly visit your nearby centre. It's Good To Know it Early When it Comes To Health! - ".CONFIG_PROJECT_NAME;

    }else if($iSMSTemplateFlag == SEND_SMS_FOR_VITAMIN_A_4_VACCINE){

        $sVaccineName = $aSMSInfo['sVaccineName'];
        $dVaccineDate = $aSMSInfo['dVaccineDate'];
        $sParentName = $aSMSInfo['sParentName'];
        $sChildNumber=  $aSMSInfo['sChildNumber'];

        $sSMSTemplate="Dear {$sParentName}, Your child {$sChildNumber}, is due for Vaccination  {$sVaccineName} on {$dVaccineDate}.Kindly visit your nearby centre. It's Good To Know it Early When it Comes To Health! - ".CONFIG_PROJECT_NAME;

    }else if($iSMSTemplateFlag == SEND_SMS_FOR_VITAMIN_A_5_VACCINE){

        $sVaccineName = $aSMSInfo['sVaccineName'];
        $dVaccineDate = $aSMSInfo['dVaccineDate'];
        $sParentName = $aSMSInfo['sParentName'];
        $sChildNumber=  $aSMSInfo['sChildNumber'];

        $sSMSTemplate="Dear {$sParentName}, Your child {$sChildNumber}, is due for Vaccination  {$sVaccineName} on {$dVaccineDate}.Kindly visit your nearby centre. It's Good To Know it Early When it Comes To Health! - ".CONFIG_PROJECT_NAME;

    }else if($iSMSTemplateFlag == SEND_SMS_FOR_VITAMIN_A_6_VACCINE){

        $sVaccineName = $aSMSInfo['sVaccineName'];
        $dVaccineDate = $aSMSInfo['dVaccineDate'];
        $sParentName = $aSMSInfo['sParentName'];
        $sChildNumber=  $aSMSInfo['sChildNumber'];

        $sSMSTemplate="Dear {$sParentName}, Your child {$sChildNumber}, is due for Vaccination  {$sVaccineName} on {$dVaccineDate}.Kindly visit your nearby centre. It's Good To Know it Early When it Comes To Health! - ".CONFIG_PROJECT_NAME;

    }else if($iSMSTemplateFlag == SEND_SMS_FOR_VITAMIN_A_7_VACCINE){

        $sVaccineName = $aSMSInfo['sVaccineName'];
        $dVaccineDate = $aSMSInfo['dVaccineDate'];
        $sParentName = $aSMSInfo['sParentName'];
        $sChildNumber=  $aSMSInfo['sChildNumber'];

        $sSMSTemplate="Dear {$sParentName}, Your child {$sChildNumber}, is due for Vaccination  {$sVaccineName} on {$dVaccineDate}.Kindly visit your nearby centre. It's Good To Know it Early When it Comes To Health! - ".CONFIG_PROJECT_NAME;

    }else if($iSMSTemplateFlag == SEND_SMS_FOR_VITAMIN_A_8_VACCINE){

        $sVaccineName = $aSMSInfo['sVaccineName'];
        $dVaccineDate = $aSMSInfo['dVaccineDate'];
        $sParentName = $aSMSInfo['sParentName'];
        $sChildNumber=  $aSMSInfo['sChildNumber'];

        $sSMSTemplate="Dear {$sParentName}, Your child {$sChildNumber}, is due for Vaccination  {$sVaccineName} on {$dVaccineDate}.Kindly visit your nearby centre. It's Good To Know it Early When it Comes To Health! - ".CONFIG_PROJECT_NAME;

    }else if($iSMSTemplateFlag == SEND_SMS_FOR_DPT_2_VACCINE){

        $sVaccineName = $aSMSInfo['sVaccineName'];
        $dVaccineDate = $aSMSInfo['dVaccineDate'];
        $sParentName = $aSMSInfo['sParentName'];
        $sChildNumber=  $aSMSInfo['sChildNumber'];

        $sSMSTemplate="Dear {$sParentName}, Your child {$sChildNumber}, is due for Vaccination  {$sVaccineName} on {$dVaccineDate}.Kindly visit your nearby centre. It's Good To Know it Early When it Comes To Health! - ".CONFIG_PROJECT_NAME;

    }else{
        $sVaccineName = $aSMSInfo['sVaccineName'];
        $dVaccineDate = $aSMSInfo['dVaccineDate'];
        $sParentName = $aSMSInfo['sParentName'];
        $sChildNumber=  $aSMSInfo['sChildNumber'];

        $sSMSTemplate="Dear {$sParentName}, Your child {$sChildNumber}, is due for Vaccination  {$sVaccineName} on {$dVaccineDate}.Kindly visit your nearby centre. It's Good To Know it Early When it Comes To Health! - ".CONFIG_PROJECT_NAME;

    }

    return $sSMSTemplate;
}
?>