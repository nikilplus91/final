<?php

//! brief function to process the SMS using web2sms api
//! Use cURL to connect with API and get response from API
function cURLProcessSMS($sTo,$sMessage){

	//! web2sms API URL
	$sApiUrl = SMS_API_URL;
	//! web2sms API working key
	$sWorkingKey = SMS_API_WORKING_KEY;
	//! web2sm API sender id
	$sSender = SMS_API_SENDER;

    //$sMessage = urlencode($sMessage);
    //! Replace the space with + to pass the message in correct form for http api
    $sMessage1 = str_replace(" ", "+", $sMessage);

	//! Make the URL to send the SMS
	$sUrl = "{$sApiUrl}workingkey={$sWorkingKey}&sender={$sSender}&to={$sTo}&message={$sMessage1}";

	//! create curl resource 
    $oCurl = curl_init(); 
    
    //! set url 
    curl_setopt($oCurl, CURLOPT_URL, $sUrl); 

    //! return the transfer as a string 
    curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1);

    //! $sOutput contains the output string 
    $sOutput = curl_exec($oCurl); 
    
    //! close curl resource to free up system resources
    curl_close($oCurl);

    return $sOutput;
}

//! brief function to process the marketing SMS...
//! Use cURL to connect with API and get response from API
function cURLProcessMarketingSMS($sTo,$sMessage){
    //! web2sms API URL
    $sApiUrl = SMS_MARKETING_API_URL;
    //! web2sms API working key
    $sWorkingKey = SMS_MARKETING_API_WORKING_KEY;
    //! web2sm API sender id
    $sSender = SMS_MARKETING_API_SENDER;

    //$sMessage = urlencode($sMessage);
    //! Replace the space with + to pass the message in correct form for http api
    $sMessage1 = str_replace(" ", "+", $sMessage);

    //! Make the URL to send the SMS
    $sUrl = "{$sApiUrl}workingkey={$sWorkingKey}&sender={$sSender}&to={$sTo}&message={$sMessage1}";

    //! create curl resource 
    $oCurl = curl_init(); 
    
    //! set url 
    curl_setopt($oCurl, CURLOPT_URL, $sUrl); 

    //! return the transfer as a string 
    curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1);

    //! $sOutput contains the output string 
    $sOutput = curl_exec($oCurl); 
    
    //! close curl resource to free up system resources
    curl_close($oCurl);

    return $sOutput;
}

//$iResult = cURLProcessSMS('9960789386','TEST SMS');
//print_r("<pre>");print_r($iResult);exit();

//! brief function to check the status of SMS using web2sms API
//! Provide Message id and check the status of message
function fCheckMessageStatus($sMessageId){

    //! web2sms API URL
    $sApiUrl = 'http://alerts.sinfini.com/api/status.php?';
    //! web2sms API working key
    $sWorkingKey = SMS_API_WORKING_KEY;
    //! web2sm API sender id
    $sSender = SMS_API_SENDER;

    //$sMessage = urlencode($sMessage);
    //! Replace the space with + to pass the message in correct form for http api
    $sMessage1 = str_replace(" ", "+", $sMessage);

    //! Make the URL to send the SMS
    $sUrl = "{$sApiUrl}workingkey={$sWorkingKey}&messageid={$sMessageId}";

    //! create curl resource 
    $oCurl = curl_init(); 
    
    //! set url 
    curl_setopt($oCurl, CURLOPT_URL, $sUrl);

    //! return the transfer as a string 
    curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1);

    //! $sOutput contains the output string 
    $sOutput = curl_exec($oCurl); 
    
    //! close curl resource to free up system resources
    curl_close($oCurl);

    return $sOutput;

}
?>