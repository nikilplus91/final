<?php
ini_set('max_execution_time', 0);
ini_set('memory_limit', '-1');

include_once "config/configApp.php";
include_once ABS_PATH_TO_APP."classes/class.DBConnManager.php";
include_once ABS_PATH_TO_APP."sendEmail.php";
include_once ABS_PATH_TO_APP."sendSMS.php";

$dDateToday = date('Y-m-d');

//! Fetching child details..
$aChildDetails = fGetAllEligibleChildListing();

//! Validation..
if(!empty($aChildDetails)){
	foreach ($aChildDetails as $iIndex => $aChild) {
		$iVaccinationID = $aChild['vaccination_id'];
		$iChildID = $aChild['child_id'];
		$iParentID = $aChild['parent_id'];
		$dVaccineDate = $aChild['ideal_date'];
		$sVaccinationName = $aChild['vaccination_name'];
		$sChildName = $aChild['child_name'];
		$sChildNumber = $aChild['child_no'];
		$dDOB = $aChild['date_of_birth'];
		$iUserID = $aChild['user_id'];
		$iNoOfChilds = $aChild['no_of_childs'];
		$sCity = $aChild['city'];
		$sState = $aChild['state'];
		$sCountry = $aChild['country'];
		$sRegion = $aChild['region'];
		$sParentName = $aChild['name'];
		$sMobileNo = $aChild['mobile'];
		$sEmailID = $aChild['email'];

		
		//! Validation for email..
		if(fGetEmailStatusForParent($dDateToday,$iParentID) && $sEmailID != "" && !is_null($sEmailID)){
			$aRecipientEID = array($sEmailID);
		    $aEmailRecipientName = array($sParentName);
		    $sSenderEID = EMAIL_SENDER_ID;
		    $sSenderEName = EMAIL_SENDER_NAME;
		    $iEmailTemplateFlag = $iVaccinationID;
		    $aAttachment = array();
		    $aEmailInfo = array(
		    	'sVaccineName' => $sVaccinationName,
				'dVaccineDate' => $dVaccineDate,
				'sParentName' => $sParentName,
				'sChildNumber' => $sChildNumber,
				'sEventTitle' => 'Reminder : To get child vaccinated',
				'sEventLocation' => $sCity,
				'sEventDetails' => 'Child vaccination reminder for Vaccination '.$sVaccinationName.' on '.$dVaccineDate.'.',
				'dEventDate' => $dVaccineDate,
		    );

		    //! Send Email..
		    $iResult = fSendEmail($aRecipientEID,$aEmailRecipientName,$iEmailTemplateFlag,$aEmailInfo,$aAttachment,$sSenderEID,$sSenderEName);
		    
		    if($iResult != 1){
		    	//! store alert..
		    	storeLogForEmailSent($iParentID,$dDateToday);
		    }
		}


		//! Validation for sms..
		if(fGetSMSStatusForParent($dDateToday,$iParentID) && $sMobileNo != "" && !is_null($sMobileNo)){

            $aRecipientNo = array($sMobileNo);
            $aRecipientName = array($sParentName);
            $iSMSTemplateFlag = $iVaccinationID;
            $aSMSInfo = array(
                'sVaccineName' => $sVaccinationName,
                'dVaccineDate' => $dVaccineDate,
                'sParentName' => $sParentName,
                'sChildNumber' => $sChildNumber
            );

            $iSenderNo = SMS_SENDER_NO;
            $sSenderName = SMS_SENDER_NAME;               
        
            //! Send SMS..
            $iResult = fSendSMS($aRecipientNo,$aRecipientName,$iSMSTemplateFlag,$aSMSInfo,$iSenderNo,$sSenderName);

            if($iResult == 1){
                //! store alert..
                storeLogForSMSSent($iParentID,$dDateToday);
            }
		}
	}
}

//! Function for fetching weather information..
function fGetAllEligibleChildListing(){
    $DBMan = new DBConnManager();
    $conn =  $DBMan->getConnInstance();
    $aChilds = array();
    
    $sIdealDateTbl = 'vaccination_ideal_date_details';
    $sVaccinationMasterTbl = 'vaccination_master';
    $sParentChildDetails = 'parent_child_details';
    $sParentMaster = 'parent_details';
    $sUserProfile = 'user_profiles';

    $sSQuery = "SELECT A.`vaccination_id`, A.`child_id`, A.`parent_id`, A.`ideal_date`, B.`vaccination_name`, C.`child_name`, C.`child_no`, C.`date_of_birth`, D.`user_id`, D.`no_of_childs`, D.`city`, D.`state`, D.`country`, D.`region`, E.`name`, E.`mobile`, E.`email` FROM `{$sIdealDateTbl}` AS A LEFT JOIN `{$sVaccinationMasterTbl}` AS B ON B.`vaccination_id` = A.`vaccination_id` LEFT JOIN `{$sParentChildDetails}` AS C ON C.`child_id` = A.`child_id` LEFT JOIN `{$sParentMaster}` AS D ON D.`parent_id` = A.`parent_id` LEFT JOIN `{$sUserProfile}` AS E ON E.`user_id` = D.`user_id` WHERE A.`deleted` = 0 AND B.`deleted` = 0 AND C.`deleted` = 0 AND D.`deleted` = 0 AND CURDATE() <= A.`ideal_date` AND DATEDIFF(A.`ideal_date`,CURDATE()) <= 2 AND E.`user_type_id` = 3";
    
    $sSQueryR = $conn->query($sSQuery);
    if($sSQueryR!==FALSE){
        while($aRow = $sSQueryR->fetch_assoc()){
        	$aRow['ideal_date'] = date(DEFINED_DATE_FORMATE, strtotime($aRow['ideal_date']));
            $aChilds[] = $aRow;
        }
    }

    return $aChilds;
}

//! Function for fetching weather information..
function fGetEmailStatusForParent($dDate,$iParentID){
    $DBMan = new DBConnManager();
    $conn =  $DBMan->getConnInstance();
    $bSendAlert = true;
    
    $sTable = 'email_alert_log';

    $sSQuery = "SELECT COUNT(*) AS iCount FROM `{$sTable}` WHERE `deleted` = 0 AND `date` = '{$dDate}' AND `parent_id` = '{$iParentID}'";
    
    $sSQueryR = $conn->query($sSQuery);
    if($sSQueryR!==FALSE){
        $aRow = $sSQueryR->fetch_assoc();
        if($aRow['iCount'] > 0){
        	$bSendAlert = false;
        }
    }

    return $bSendAlert;
}

//! Function for fetching weather information..
function fGetSMSStatusForParent($dDate,$iParentID){
    $DBMan = new DBConnManager();
    $conn =  $DBMan->getConnInstance();
    $bSendAlert = true;
    
    $sTable = 'sms_alert_log';

    $sSQuery = "SELECT COUNT(*) AS iCount FROM `{$sTable}` WHERE `deleted` = 0 AND `date` = '{$dDate}' AND `parent_id` = '{$iParentID}'";
    
    $sSQueryR = $conn->query($sSQuery);
    if($sSQueryR!==FALSE){
        $aRow = $sSQueryR->fetch_assoc();
        if($aRow['iCount'] > 0){
        	$bSendAlert = false;
        }
    }

    return $bSendAlert;
}

//! Store log for email sent..
function storeLogForEmailSent($iParentID,$dDate){

    $DBMan = new DBConnManager();
    $conn =  $DBMan->getConnInstance();

    $iInsertID = 0;

    $sTable = 'email_alert_log';
    $dAddedOn = date("Y-m-d H:i:s");
    
    $sIQuery = "INSERT INTO `{$sTable}` (`id`,`parent_id`,`date`,`added_on`,`deleted`)
            VALUES (NULL,'{$iParentID}','{$dDate}','{$dAddedOn}',0)";
    
    if($conn != false){
        $sIQueryR = $conn->query($sIQuery);
        if($sIQueryR > 0){
            $iInsertID = $conn->insert_id;
        }
    }

    return $iInsertID;
}

//! Store log for sms sent..
function storeLogForSMSSent($iParentID,$dDate){

    $DBMan = new DBConnManager();
    $conn =  $DBMan->getConnInstance();

    $iInsertID = 0;

    $sTable = 'sms_alert_log';
    $dAddedOn = date("Y-m-d H:i:s");
    
    $sIQuery = "INSERT INTO `{$sTable}` (`id`,`parent_id`,`date`,`added_on`,`deleted`)
            VALUES (NULL,'{$iParentID}','{$dDate}','{$dAddedOn}',0)";
    
    if($conn != false){
        $sIQueryR = $conn->query($sIQuery);
        if($sIQueryR > 0){
            $iInsertID = $conn->insert_id;
        }
    }

    return $iInsertID;
}
?>