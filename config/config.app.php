<?php
//! Set project name
define('CONFIG_PROJECT_NAME', 'Tika On Time');

//! Set organization logo
define('PROJECT_LOGO', 'org_logo.jpg');

//! Absolute path to app
define('ABS_PATH_TO_APP','/var/www/vaccine/');

//! define site url path
define('SITE_URL', 'http://localhost:96/vaccine/');

//! Set Organisation Logo URL
define('ORG_LOGO_URL', 'http://localhost:96/vaccine/images/org_logo.jpg');

//! define date format 
define('DEFINED_DATE_FORMATE', 'd-m-Y');

//! A unique Medixcel key for multiple installation on same domain
define('APP_UNIQUE_KEY', 'unbsdbgmasdklbase');

//! Set Default Timezone
define('DATE_DEFAULT_TIMEZONE', 'Asia/Kolkata');

//! Set UI date format for JS datepicker
define('DEFINED_DATE_FORMAT_FOR_JS_DATEPICKER', 'DD-MM-YYYY');

//! Email address of org's admin so we can send email for activities
define('ORG_ADMIN_EMAIL_ID', 'saurabh.chinchamalatpure@plus91.in');

//! The session Duration in Mins
define('SESSION_TIME',20);
?>